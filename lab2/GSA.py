import pickle

# Process input
ntsyms = ('<%>',) + tuple(input().split()[1:])
tsyms = tuple(input().split()[1:])
syms = ntsyms + tsyms
syncsyms = set(input().split()[1:])

prods = {'<%>': [ (ntsyms[1],) ]}
empty = set()

i = 0
prodIdx = {}

line = input()
while line:
    ntsym = line
    line = input()
    while line and line[0] == ' ':
        prod = tuple(line.split()) if line != ' $' else ()
        
        prods.setdefault(ntsym, []).append(prod)
        prodIdx[ntsym, prod] = i

        i += 1
        try:
            line = input()
        except EOFError:
            line = None
            break

##print(prods)

# Calculate empty symbols
dirty = True
while dirty:
    dirty = False
    for ntsym, prodl in prods.items():
        if ntsym not in empty:
            for prod in prodl:
                if all(map(lambda sym: sym in empty, prod)):
                    empty.add(ntsym)
                    dirty = True
                    break

print(empty)

##startsWithSymD = {}
##for sym, prodl in prods.items():
##    s = startsWithSymD.setdefault(sym, set())
##    for prod in prodl:
##        for zym in prod:
##            s.add(zym)
##            if zym not in empty:
##                break
##
##print(startsWithSymD)

# Calculate StartsWithSymbol structure
startsWithSym = {sym: {sym} for sym in syms}
dirty = True
while dirty:
    dirty = False
    for ntsym, prodl in prods.items():
        s = startsWithSym[ntsym]
        for prod in prodl:
            for zym in prod:
                diff = startsWithSym[zym] - s
                if diff:
                    s |= diff
                    dirty = True
                if zym not in empty:
                    break

##print(startsWithSym)

# Calculate StartsWithTerminalSymbol structure
tsymSet = set(tsyms)
ntsymSet = set(ntsyms)
startsWithTsym = {sym: s & tsymSet for sym, s in startsWithSym.items()}

##print(startsWithTsym)

# Define StartsWith sets
def startsWith(string):
    s = set()
    for sym in string:
        s |= startsWithTsym[sym]
        if sym not in empty:
            break
    return s

# Construct e-NKA
enka = {}

def fillEnka(ntsym, prod, pos, s):
    state = ntsym, prod, pos, s
    if state in enka:
        return
    
    trans = enka.setdefault(state, {})
    
    if pos < len(prod):
        sym = prod[pos]
        rest = prod[pos + 1:]
        
        nxt = (ntsym, prod, pos + 1, s)
        trans[prod[pos]] = nxt
        fillEnka(*nxt)

        if sym in ntsymSet:
            t = startsWith(rest)
            if (all(map(lambda sym: sym in empty, rest))):
                t |= s
            t = frozenset(t)

            l = []
            for prod in prods[sym]:
                nxt = (sym, prod, 0, t)
                l.append(nxt)
                fillEnka(*nxt)
            trans['#'] = tuple(l)

START = '<%>', (ntsyms[1],), 0, frozenset({'#'})

fillEnka(*START)

##print()
print(len(enka))
##for state, d in enka.items():
##    print(state)
##    for sym, nxt in d.items():
##        print('\t' + sym, '->', nxt)

# Construct DKA
eclosures = {}

def eclosure_fill(u, s):
    if u in s:
        return
    if u in eclosures:
        s |= eclosures[u]
        return
    s.add(u)
    for v in enka[u].get('#', ()):
        eclosure_fill(v, s)

for state in enka:
    s = set()
    eclosure_fill(state, s)
    s = frozenset(s)
    eclosures[state] = s

dka = {}
def fillDka(states):
    if states in dka:
        return

    d = dka.setdefault(states, {})
    for state in states:
        for k, v in enka[state].items():
            if k != '#':
                d.setdefault(k, set()).add(v)
    
    for k, v in d.copy().items():
        s = set()
        for state in v:
            if state not in s:
                s |= eclosures[state]
        s = frozenset(s)
        
        d[k] = s
        fillDka(s)

q0 = eclosures[START]
fillDka(q0)

##print()
print(len(dka))
##for state, d in dka.items():
##    print(state)
##    for sym, nxt in d.items():
##        print('\t' + sym, '->', nxt)

# Enumerate states
index = {s: i for i, s in enumerate(dka)}

print(index[q0])

# Construct Action table
action = []
for state, trans in dka.items():
    d = {}
    causes = {}
    for ntsym, prod, pos, s in state:
        if pos < len(prod):
            sym = prod[pos]
            if sym in tsymSet:
                nxt = trans.get(sym)
                if nxt:
                    d[sym] = (0, index[nxt])
                    causes[sym] = ntsym, prod
        else:
            if ntsym != '<%>':
                for sym in s:
                    if sym in d:
                        if d[sym][0] == 0:
                            print("Shift/Reduce conflict at", sym)
                            print(" between", causes[sym][0], "->", causes[sym][1])
                            print(" and", ntsym, "->", prod)
                            continue
                        else:
                            print("Reduce/Reduce conflict at", sym)
                            print(" between", d[sym][1], "->", d[sym][2])
                            print(" and", ntsym, "->", prod)
                            if prodIdx[d[sym][1:]] < prodIdx[ntsym, prod]:
                                continue
                    d[sym] = (1, ntsym, prod)
            else:
                d['#'] = (3,)
    action.append(d)

# Construct NextState table
next_state = []
for state, trans in dka.items():
    d = {}
    for sym, nxt in trans.items():
        if sym in ntsymSet:
            d[sym] = index[nxt]
    next_state.append(d)

# Output data to analyzer
data = (syncsyms, index[q0], tuple(action), tuple(next_state))
with open('analizator/config.bin', 'wb') as f:
    pickle.dump(data, f, protocol=4)
