from sys import argv, stderr
from pathlib import Path
from subprocess import run, PIPE

src = Path(argv[1])
root = Path('..')

if len(argv) > 2:
    with open('ppjC.lan') as f:
        run(['python', 'GLA.py'], cwd = root / 'lab1', stdin = f)

with src.open() as fin, src.with_suffix('.in').open('w') as fout:
    run(['python','LA.py'], cwd = root / 'lab1' / 'analizator', stdin = fin, stdout = fout)

if len(argv) > 2:
    with open('ppjC.san') as f:
        run(['python','GSA.py'], cwd = root / 'lab2', stdin = f)

with src.with_suffix('.in').open() as fin, src.with_suffix('.out').open('w') as fout:
    run(['python','SA.py'], cwd = root / 'lab2' / 'analizator', stdin = fin, stdout = fout)

with src.with_suffix('.out').open() as f:
    proc = run(['./lab3'], cwd = root / 'lab3' / 'bin' / 'Debug', stdin = f)
    proc.check_returncode()
