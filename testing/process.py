from sys import argv, stderr
from pathlib import Path
from subprocess import run, PIPE

src = Path(argv[1])
root = Path('..')

if len(argv) > 2:
    with open('ppjC.lan') as f:
        run(['py', '-3', 'GLA.py'], shell = True, cwd = root / 'lab1', stdin = f)

with src.open() as fin, src.with_suffix('.in').open('w') as fout:
    run(['py', '-3', 'LA.py'], shell = True, cwd = root / 'lab1' / 'analizator', stdin = fin, stdout = fout)

if len(argv) > 2:
    with open('ppjC.san') as f:
        run(['py', '-3', 'GSA.py'], shell = True, cwd = root / 'lab2', stdin = f)

with src.with_suffix('.in').open() as fin, src.with_suffix('.out').open('w') as fout:
    run(['py', '-3', 'SA.py'], shell = True, cwd = root / 'lab2' / 'analizator', stdin = fin, stdout = fout)

with src.with_suffix('.out').open() as f:
    proc = run(['lab4.exe'], shell = True, cwd = root / 'lab4' / 'bin' / 'Release', stdin = f)
    proc.check_returncode()
