#pragma once
#include "tree.h"

struct slozena_naredba: scope, std::enable_shared_from_this<slozena_naredba> {
	void enclose_child(std::shared_ptr<tree_node> child) override;

	void check();
};

struct lista_naredbi: tree_node {
	void check();
};

struct naredba: tree_node {
	void check();
};

struct izraz_naredba: tree_node {
    std::shared_ptr<ppjc_type> type;
	void check();
};

struct naredba_grananja: scope, std::enable_shared_from_this<naredba_grananja> {
	void enclose_child(std::shared_ptr<tree_node> child) override;

	void check();
};

struct naredba_petlje: scope, std::enable_shared_from_this<naredba_petlje> {
	void enclose_child(std::shared_ptr<tree_node> child) override;

	void check();
};

struct naredba_skoka: tree_node {
	void check();
};

struct prijevodna_jedinica: scope, std::enable_shared_from_this<prijevodna_jedinica> {
    std::vector<std::pair<std::string, function_type>> declared_functions;

	void enclose_child(std::shared_ptr<tree_node> child) override;

	void check();
};

struct vanjska_deklaracija: tree_node {
	void check();
};

