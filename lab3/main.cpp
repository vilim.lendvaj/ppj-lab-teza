#include <iostream>
#include <cassert>

#include "parser.h"
#include "statements.h"

//using namespace std;

int main()
{
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(nullptr);

	std::string str;
	getline(std::cin, str);
	assert(str == "<prijevodna_jedinica>");
	std::shared_ptr<prijevodna_jedinica> root = std::dynamic_pointer_cast<prijevodna_jedinica>(make_node(nullptr, nullptr, str, 1));
	root->declared_functions.emplace_back("main", function_type(int_t, std::vector<std::shared_ptr<ppjc_type>>{}));
	std::shared_ptr<tree_node> node = root;
	int level = 1, id = 2;

	while (getline(std::cin, str) && !str.empty()) {
		int new_level = str.find_first_not_of(" ");
		while (new_level < level) {
			//cout<<node->str<<": "<<node->children.size()<<'\n';
			node = node->parent;
			--level;
		}
		node->children.push_back(make_node(root, node, str.substr(level), id));
		node = node->children.back();
		++level;
		++id;
	}

	root->calculate_scopes();
	root->check();

	for (const auto& x: root->declared_functions) {
        auto *found = root->find(x.first);
        if (!found
            || typeid(*found->type) != typeid(function_type)
            || *std::dynamic_pointer_cast<function_type>(found->type) != x.second
            || !found->defined) {

            std::cout<<(x.first == "main" ? "main" : "funkcija")<<std::endl;
            exit(0);
        }
	}



    //std::cout<<"all ok"<<std::endl;
    return 0;
}

