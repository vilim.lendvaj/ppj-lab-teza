#include "terminals.h"
#include "expressions.h"
#include "statements.h"
#include "decl_def.h"
#include "types.h"


void primarni_izraz::check() {
    if (children.size() == 3) {
        //<primarni_izraz> ::= L_ZAGRADA <izraz> D_ZAGRADA
        auto child = std::dynamic_pointer_cast<izraz>(children[1]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    } else {
        if (typeid(*children[0]) == typeid(BROJ)) {
            //<primarni_izraz> ::= BROJ
            auto child = std::dynamic_pointer_cast<BROJ>(children[0]);
            child->check();

            if (!child->is_valid)
                error();

            type = child->type;
            lvalue = false;
        }
        else if (typeid(*children[0]) == typeid(ZNAK)) {
            //<primarni_izraz> ::= ZNAK
            auto child = std::dynamic_pointer_cast<ZNAK>(children[0]);
            child->check();

            if (!child->is_valid)
                error();

            type = child->type;
            lvalue = false;
        }
        else if (typeid(*children[0]) == typeid(NIZ_ZNAKOVA)) {
            //<primarni_izraz> ::= NIZ_ZNAKOVA
            auto child = std::dynamic_pointer_cast<NIZ_ZNAKOVA>(children[0]);
            child->check();

            if (!child->is_valid)
                error();

            type = child->type;
            lvalue = false;
        }
        else {
            //<primarni_izraz> ::= IDN
            auto child = std::dynamic_pointer_cast<IDN>(children[0]);
            child->check();

            if (!child->declared)
                error();

            type = child->type;
            lvalue = child->lvalue;
        }
    }
}

void postfiks_izraz::check() {
    if (children.size() == 1) {
        //<postfiks_izraz> ::= <primarni_izraz>
        auto child = std::dynamic_pointer_cast<primarni_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    }
    else if(children.size() == 4 && children[2]->str == "<izraz>"){
        //<postfiks_izraz> ::= <postfiks_izraz> L_UGL_ZAGRADA <izraz> D_UGL_ZAGRADA
        auto postfiks_izraz_child = std::dynamic_pointer_cast<postfiks_izraz>(children[0]);
        postfiks_izraz_child->check();

        if(*postfiks_izraz_child->type != array_type(int_t)  &&
           *postfiks_izraz_child->type != array_type(char_t) &&
           *postfiks_izraz_child->type != array_type(cint_t) &&
           *postfiks_izraz_child->type != array_type(cchar_t) )
            error();

        auto izraz_child = std::dynamic_pointer_cast<izraz>(children[2]);
        izraz_child->check();

        if(!izraz_child->type->is_convertible(int_t))
            error();

        auto postfiks_izraz_child_type = std::dynamic_pointer_cast<array_type>(postfiks_izraz_child->type);
        type = std::make_shared<primitive_type>(postfiks_izraz_child_type->base_type);

        lvalue = (postfiks_izraz_child_type->base_type.is_const ? false : true);

    }
    else if(children.size() == 3){
        //<postfiks_izraz> ::= <postfiks_izraz> L_ZAGRADA D_ZAGRADA
        auto postfiks_izraz_child = std::dynamic_pointer_cast<postfiks_izraz>(children[0]);
        postfiks_izraz_child->check();

        if(typeid(*postfiks_izraz_child->type) != typeid(function_type))
            error();

        auto postfiks_izraz_child_type = std::dynamic_pointer_cast<function_type>(postfiks_izraz_child->type);

        if(postfiks_izraz_child_type->parameters.size() != 0)
            error();

        type = std::make_shared<primitive_type>(postfiks_izraz_child_type->return_type);
        lvalue = false;
    }
    else if(children.size() == 4 && children[2]->str == "<lista_argumenata>"){
        //<postfiks_izraz> ::= <postfiks_izraz> L_ZAGRADA <lista_argumenata> D_ZAGRADA
        auto postfiks_izraz_child = std::dynamic_pointer_cast<postfiks_izraz>(children[0]);
        postfiks_izraz_child->check();

        auto lista_argumenata_child = std::dynamic_pointer_cast<lista_argumenata>(children[2]);
        lista_argumenata_child->check();

        if(typeid(*postfiks_izraz_child->type) != typeid(function_type))
            error();

        auto postfiks_izraz_child_type = std::dynamic_pointer_cast<function_type>(postfiks_izraz_child->type);

        if(postfiks_izraz_child_type->parameters.size() != lista_argumenata_child->types.size())
            error();

        for(unsigned int i = 0; i < lista_argumenata_child->types.size(); i++) {
            if(!lista_argumenata_child->types[i]->is_convertible(*postfiks_izraz_child_type->parameters[i]))
                error();
        }

        type = std::make_shared<primitive_type>(postfiks_izraz_child_type->return_type);
        lvalue = false;
    }
    else if(children.size() == 2){
        //<postfiks_izraz> ::= <postfiks_izraz> (OP_INC | OP_DEC)
        auto postfix_izraz_child = std::dynamic_pointer_cast<postfiks_izraz>(children[0]);
        postfix_izraz_child->check();

        if(postfix_izraz_child->lvalue == 0 || !postfix_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;
    }
}

void lista_argumenata::check() {
    if (children.size() == 1){
        //<lista_argumenata> ::= <izraz_pridruzivanja>
        auto izraz_pridruzivanja_child = std::dynamic_pointer_cast<izraz_pridruzivanja>(children[0]);
        izraz_pridruzivanja_child->check();

        types.clear();      // just in case
        types.push_back(izraz_pridruzivanja_child->type);
    }
    else if(children.size() == 3){
        //<lista_argumenata> ::= <lista_argumenata> ZAREZ <izraz_pridruzivanja>
        auto lista_argumenata_child = std::dynamic_pointer_cast<lista_argumenata>(children[0]);
        lista_argumenata_child->check();

        auto izraz_pridruzivanja_child = std::dynamic_pointer_cast<izraz_pridruzivanja>(children[2]);
        izraz_pridruzivanja_child->check();

        copy(lista_argumenata_child->types.begin(), lista_argumenata_child->types.end(), back_inserter(types));

        types.push_back(izraz_pridruzivanja_child->type);
    }
}

void unarni_izraz::check() {
    if (children.size() == 1) {
        //<unarni_izraz> ::= <postfiks_izraz>
        auto child = std::dynamic_pointer_cast<postfiks_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    }
    else if (children.size() == 2 && children[1]->str == "<unarni_izraz>") {
        //(<unarni_izraz> ::= OP_INC | OP_DEC) <unarni_izraz>
        auto unarni_izraz_child = std::dynamic_pointer_cast<unarni_izraz>(children[1]);
        unarni_izraz_child->check();

        if(unarni_izraz_child->lvalue == false || !unarni_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;
    }
    else if (children.size() == 2 && children[0]->str == "<unarni_operator>") {
        auto cast_izraz_child = std::dynamic_pointer_cast<cast_izraz>(children[1]);
        cast_izraz_child->check();

        if(!cast_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;
    }
}

void unarni_operator::check() {

}

void cast_izraz::check() {
    if (children.size() == 1) {
        //<cast_izraz> ::= <unarni_izraz>
        auto child = std::dynamic_pointer_cast<unarni_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    }
    else if (children.size() == 4) {
        //<cast_izraz> ::= L_ZAGRADA <ime_tipa> D_ZAGRADA <cast_izraz>
        auto ime_tipa_child = std::dynamic_pointer_cast<ime_tipa>(children[1]);
        ime_tipa_child->check();

        auto cast_izraz_child = std::dynamic_pointer_cast<cast_izraz>(children[3]);
        cast_izraz_child->check();

        if (!cast_izraz_child->type->is_explicitly_convertible(*ime_tipa_child->type))        //POGLEDAJ DODATNO
            error();

        type = ime_tipa_child->type;
        lvalue = false;
    }
}

void ime_tipa::check() {
    if (children.size() == 1){
        //<ime_tipa> ::= <specifikator_tipa>
        auto specifikator_tipa_ptr = std::dynamic_pointer_cast<specifikator_tipa>(children[0]);
        specifikator_tipa_ptr->check();
        type = specifikator_tipa_ptr->type;
    }
    else if (children.size() == 2){
        //<ime_tipa> ::= KR_CONST <specifikator_tipa>
        auto specifikator_tipa_ptr = std::dynamic_pointer_cast<specifikator_tipa>(children[1]);
        specifikator_tipa_ptr->check();
        if (*specifikator_tipa_ptr->type == void_t)
            error();

        type = std::make_shared<primitive_type>(specifikator_tipa_ptr->type->name, true);     //convert type to const
    }
}

void specifikator_tipa::check() {
    children[0]->check();

    //<specifikator_tipa> ::= KR_VOID
    if (typeid(*children[0]) == typeid(KR_VOID))
        type = std::dynamic_pointer_cast<KR_VOID>(children[0])->type;

	//<specifikator_tipa> ::= KR_CHAR
    else if(typeid(*children[0]) == typeid(KR_CHAR))
        type = std::dynamic_pointer_cast<KR_CHAR>(children[0])->type;

	//<specifikator_tipa> ::= KR_INT
    else if(typeid(*children[0]) == typeid(KR_INT))
        type = std::dynamic_pointer_cast<KR_INT>(children[0])->type;
}

void multiplikativni_izraz::check() {
    if (children.size() == 1) {
        //<multiplikativni_izraz> ::= <cast_izraz>
        auto child = std::dynamic_pointer_cast<cast_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    }
    else if (children.size() == 3) {
        //<multiplikativni_izraz> ::= <multiplikativni_izraz> (OP_PUTA | OP_DIJELI | OP_MOD) <cast_izraz>
        auto multiplikativni_izraz_child = std::dynamic_pointer_cast<multiplikativni_izraz>(children[0]);
        multiplikativni_izraz_child->check();

        if(!multiplikativni_izraz_child->type->is_convertible(int_t))
            error();

        auto cast_izraz_child = std::dynamic_pointer_cast<cast_izraz>(children[2]);
        cast_izraz_child->check();

        if(!cast_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;
    }
}

void aditivni_izraz::check() {
    if (children.size() == 1) {
        //<aditivni_izraz> ::= <multiplikativni_izraz>
        auto child = std::dynamic_pointer_cast<multiplikativni_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    }
    else if (children.size() == 3) {
        //<aditivni_izraz> ::= <aditivni_izraz> (PLUS | MINUS) <multiplikativni_izraz>

        auto aditivni_izraz_child = std::dynamic_pointer_cast<aditivni_izraz>(children[0]);
        aditivni_izraz_child->check();

        if(!aditivni_izraz_child->type->is_convertible(int_t))
            error();

        auto multiplikativni_izraz_child = std::dynamic_pointer_cast<multiplikativni_izraz>(children[2]);
        multiplikativni_izraz_child->check();

        if(!multiplikativni_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;
    }
}

void odnosni_izraz::check() {
    if (children.size() == 1) {
        //<odnosni_izraz> ::= <aditivni_izraz>
        auto child = std::dynamic_pointer_cast<aditivni_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    }
    else if (children.size() == 3){
        //<odnosni_izraz> ::= <odnosni_izraz> (OP_LT | OP_GT | OP_LTE | OP_GTE) <aditivni_izraz>

        auto odnosni_izraz_child = std::dynamic_pointer_cast<odnosni_izraz>(children[0]);
        odnosni_izraz_child->check();

        if(!odnosni_izraz_child->type->is_convertible(int_t))
            error();

        auto aditivni_izraz_child = std::dynamic_pointer_cast<aditivni_izraz>(children[2]);
        aditivni_izraz_child->check();

        if(!aditivni_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;
    }
}

void jednakosni_izraz::check() {
    if (children.size() == 1) {
        //<jednakosni_izraz> ::= <odnosni_izraz>
        auto child = std::dynamic_pointer_cast<odnosni_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    }
    else if (children.size() == 3){
        //<jednakosni_izraz> ::= <jednakosni_izraz> (OP_EQ | OP_NEQ) <odnosni_izraz>
        auto jednakosni_izraz_child = std::dynamic_pointer_cast<jednakosni_izraz>(children[0]);
        jednakosni_izraz_child->check();

        if(!jednakosni_izraz_child->type->is_convertible(int_t))
            error();

        auto odnosni_izraz_child = std::dynamic_pointer_cast<odnosni_izraz>(children[2]);
        odnosni_izraz_child->check();

        if(!odnosni_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;
    }
}

void bin_i_izraz::check() {
    if (children.size() == 1) {
        //<bin_i_izraz> ::= <jednakosni_izraz>
        auto child = std::dynamic_pointer_cast<jednakosni_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    }
    else if (children.size() == 3) {
        //<bin_i_izraz> ::= <bin_i_izraz> OP_BIN_I <jednakosni_izraz>
        auto bin_i_izraz_child = std::dynamic_pointer_cast<bin_i_izraz>(children[0]);
        bin_i_izraz_child->check();

        if(!bin_i_izraz_child->type->is_convertible(int_t))
            error();

        auto jednakosni_izraz_child = std::dynamic_pointer_cast<jednakosni_izraz>(children[2]);
        jednakosni_izraz_child->check();

        if(!jednakosni_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;
    }
}

void bin_xili_izraz::check() {
    if (children.size() == 1) {
        //<bin_xili_izraz> ::= <bin_i_izraz>
        auto child = std::dynamic_pointer_cast<bin_i_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    }
    else if (children.size() == 3) {
        //<bin_xili_izraz> ::= <bin_xili_izraz> OP_BIN_XILI <bin_i_izraz>
        auto bin_xili_izraz_child = std::dynamic_pointer_cast<bin_xili_izraz>(children[0]);
        bin_xili_izraz_child->check();

        if(!bin_xili_izraz_child->type->is_convertible(int_t))
            error();

        auto bin_i_izraz_child = std::dynamic_pointer_cast<bin_i_izraz>(children[2]);
        bin_i_izraz_child->check();

        if(!bin_i_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;
    }
}

void bin_ili_izraz::check() {
    if (children.size() == 1) {
        //<bin_ili_izraz> ::= <bin_xili_izraz>
        auto child = std::dynamic_pointer_cast<bin_xili_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    }
    else if (children.size() == 3) {
        //<bin_ili_izraz> ::= <bin_ili_izraz> OP_BIN_ILI <bin_xili_izraz>
        auto bin_ili_izraz_child = std::dynamic_pointer_cast<bin_ili_izraz>(children[0]);
        bin_ili_izraz_child->check();

        if(!bin_ili_izraz_child->type->is_convertible(int_t))
            error();

        auto bin_xili_izraz_child = std::dynamic_pointer_cast<bin_xili_izraz>(children[2]);
        bin_xili_izraz_child->check();

        if(!bin_xili_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;
    }
}

void log_i_izraz::check() {
    if (children.size() == 1) {
        //<log_i_izraz> ::= <bin_ili_izraz>
        auto child = std::dynamic_pointer_cast<bin_ili_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    }
    else if (children.size() == 3) {
        //<log_i_izraz> ::= <log_i_izraz> OP_I <bin_ili_izraz>
        auto log_ili_izraz_child = std::dynamic_pointer_cast<log_i_izraz>(children[0]);
        log_ili_izraz_child->check();

        if(!log_ili_izraz_child->type->is_convertible(int_t))
            error();

        auto bin_ili_izraz_child = std::dynamic_pointer_cast<bin_ili_izraz>(children[2]);
        bin_ili_izraz_child->check();

        if(!bin_ili_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;
    }
}

void log_ili_izraz::check() {
    if (children.size() == 1) {
        //<log_ili_izraz> ::= <log_i_izraz>
        auto child = std::dynamic_pointer_cast<log_i_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    }
    else if (children.size() == 3) {
        //<log_ili_izraz> ::= <log_ili_izraz> OP_ILI <log_i_izraz>
        auto log_ili_izraz_child = std::dynamic_pointer_cast<log_ili_izraz>(children[0]);
        log_ili_izraz_child->check();

        if(!log_ili_izraz_child->type->is_convertible(int_t))
            error();

        auto log_i_izraz_child = std::dynamic_pointer_cast<log_i_izraz>(children[2]);
        log_i_izraz_child->check();

        if(!log_i_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;
    }
}

void izraz_pridruzivanja::check() {
    if (children.size() == 1) {
        //<izraz_pridruzivanja> ::= <log_ili_izraz>
        auto child = std::dynamic_pointer_cast<log_ili_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    }
    else if (children.size() == 3) {
        //<izraz_pridruzivanja> ::= <postfiks_izraz> OP_PRIDRUZI <izraz_pridruzivanja>
        //std::cout<<"usao u izraz pridruzivanja"<<std::endl;
        auto postfiks_izraz_child = std::dynamic_pointer_cast<postfiks_izraz>(children[0]);
        postfiks_izraz_child->check();

        if (postfiks_izraz_child->lvalue != true)
            error();

        auto izraz_pridruzivanja_child = std::dynamic_pointer_cast<izraz_pridruzivanja>(children[2]);
        izraz_pridruzivanja_child->check();

        if (!izraz_pridruzivanja_child->type->is_convertible(*postfiks_izraz_child->type))
            error();

        type = postfiks_izraz_child->type;
        lvalue = false;
        //std::cout<<"izasao iz izraza pridruzivanja"<<std::endl;
    }
}

void izraz::check() {
    if (children.size() == 1) {
        //<izraz> ::= <izraz_pridruzivanja>
        auto child = std::dynamic_pointer_cast<izraz_pridruzivanja>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
    }
    else if (children.size() == 3) {
        //<izraz> ::= <izraz> ZAREZ <izraz_pridruzivanja>
        auto izraz_child = std::dynamic_pointer_cast<izraz>(children[0]);
        izraz_child->check();

        auto izraz_pridruzivanja_child = std::dynamic_pointer_cast<izraz_pridruzivanja>(children[2]);
        izraz_pridruzivanja_child->check();

        type = izraz_pridruzivanja_child->type;
        lvalue = false;
    }
}

