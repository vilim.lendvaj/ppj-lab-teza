#include <iostream>
#include <algorithm>

#include "terminals.h"
#include "expressions.h"
#include "statements.h"
#include "decl_def.h"

void definicija_funkcije::enclose_child(std::shared_ptr<tree_node> child) {
    child->enclosing_scope = shared_from_this();
    child->enclosing_function = shared_from_this();
    child->enclosing_loop = nullptr;
}

void definicija_funkcije::check() {
    if (typeid(*children[3]) == typeid(KR_VOID)) {

        //<definicija_funkcije> ::= <ime_tipa> IDN L_ZAGRADA KR_VOID D_ZAGRADA <slozena_naredba>
        auto ime_tipa_ptr = std::dynamic_pointer_cast<ime_tipa>(children[0]);
        ime_tipa_ptr->check();

        if (*ime_tipa_ptr->type == cchar_t || *ime_tipa_ptr->type == cint_t)
            error();

        this->type = std::make_shared<function_type>(*ime_tipa_ptr->type, std::vector<std::shared_ptr<ppjc_type>>{});

        auto IDN_ptr = std::dynamic_pointer_cast<IDN>(children[1]);
        IDN_ptr->check();

        if (IDN_ptr->declared && (IDN_ptr->defined || *IDN_ptr->type != *this->type))
            error();

        if (IDN_ptr->declared) {
            IDN_ptr->entry->defined = true;
        } else {
            root->declare(IDN_ptr->value, symbol_entry{this->type, true});
        }

        children[5]->check();
    }
    else if (children[3]->str == "<lista_parametara>") {
        //<definicija_funkcije> ::= <ime_tipa> IDN L_ZAGRADA <lista_parametara> D_ZAGRADA <slozena_naredba>
        auto ime_tipa_ptr = std::dynamic_pointer_cast<ime_tipa>(children[0]);
        ime_tipa_ptr->check();

        if (*ime_tipa_ptr->type == cchar_t || *ime_tipa_ptr->type == cint_t)
            error();

        this->type = std::make_shared<function_type>(*ime_tipa_ptr->type, std::vector<std::shared_ptr<ppjc_type>>{});

        auto IDN_ptr = std::dynamic_pointer_cast<IDN>(children[1]);
        IDN_ptr->check();

        if (IDN_ptr->declared && IDN_ptr->defined)
            error();

        auto lista_parametara_child = std::dynamic_pointer_cast<lista_parametara>(children[3]);
        lista_parametara_child->check();

        this->type = std::make_shared<function_type>(*ime_tipa_ptr->type, lista_parametara_child->types);

        if (IDN_ptr->declared && *IDN_ptr->type != *this->type)
            error();

        if (IDN_ptr->declared) {
            IDN_ptr->entry->defined = true;
        } else {
            root->declare(IDN_ptr->value, symbol_entry{this->type, true});
        }

        //WARNING MOZDA SKROZ KRIVO
        for (unsigned i = 0; i < lista_parametara_child->names.size(); i++) {
            this->declare(lista_parametara_child->names[i], symbol_entry{lista_parametara_child->types[i], true});
        }

        children[5]->check();
    }
};

void lista_parametara::check() {
    if (children.size() == 1) {
        //<lista_parametara> ::= <deklaracija_parametra>
        auto deklaracija_parametra_child = std::dynamic_pointer_cast<deklaracija_parametra>(children[0]);
        deklaracija_parametra_child->check();

        types.clear();
        names.clear();

        types.push_back(deklaracija_parametra_child->type);
        names.push_back(deklaracija_parametra_child->name);
    }
    else if (children.size() == 3) {
        //<lista_parametara> ::= <lista_parametara> ZAREZ <deklaracija_parametra>
        auto lista_parametara_child = std::dynamic_pointer_cast<lista_parametara>(children[0]);
        lista_parametara_child->check();

        auto deklaracija_parametra_child = std::dynamic_pointer_cast<deklaracija_parametra>(children[2]);
        deklaracija_parametra_child->check();

        types = lista_parametara_child->types;
        names = lista_parametara_child->names;

        if (std::find(names.begin(), names.end(), deklaracija_parametra_child->name) != names.end())
            error();

        types.push_back(deklaracija_parametra_child->type);
        names.push_back(deklaracija_parametra_child->name);
    }
};

void deklaracija_parametra::check() {
    if (children.size() == 2) {
        //<deklaracija_parametra> ::= <ime_tipa> IDN
        auto ime_tipa_child = std::dynamic_pointer_cast<ime_tipa>(children[0]);
        ime_tipa_child->check();

        if(*ime_tipa_child->type == void_t)
            error();

        auto IDN_child = std::dynamic_pointer_cast<IDN>(children[1]);
                     //IDN_child->check();

        type = ime_tipa_child->type;
        name = IDN_child->value;
    }
    else if (children.size() == 4) {
        //<deklaracija_parametra> ::= <ime_tipa> IDN L_UGL_ZAGRADA D_UGL_ZAGRADA
        auto ime_tipa_child = std::dynamic_pointer_cast<ime_tipa>(children[0]);
        ime_tipa_child->check();

        if(*ime_tipa_child->type == void_t)
            error();

        auto IDN_child = std::dynamic_pointer_cast<IDN>(children[1]);
                    //IDN_child->check();

        type = std::make_shared<array_type>(*ime_tipa_child->type);
        name = IDN_child->value;
    }
};

void lista_deklaracija::check() {
    if (children.size() == 1) {
        //<lista_deklaracija> ::= <deklaracija>
        auto deklaracija_child = std::dynamic_pointer_cast<deklaracija>(children[0]);
        deklaracija_child->check();
    }
    else if (children.size() == 2) {
        //<lista_deklaracija> ::= <lista_deklaracija> <deklaracija>
        auto lista_deklaracija_child = std::dynamic_pointer_cast<lista_deklaracija>(children[0]);
        lista_deklaracija_child->check();

        auto deklaracija_child = std::dynamic_pointer_cast<deklaracija>(children[1]);
        deklaracija_child->check();
    }
};

void deklaracija::check() {
    //<deklaracija> ::= <ime_tipa> <lista_init_deklaratora> TOCKAZAREZ
    auto type = std::dynamic_pointer_cast<ime_tipa>(children[0]);
    auto declarators = std::dynamic_pointer_cast<lista_init_deklaratora>(children[1]);
    type->check();
    declarators->ntype = type->type;
    declarators->check();
};

void lista_init_deklaratora::check() {
    if (children.size() == 1) {
        //<lista_init_deklaratora> ::= <init_deklarator>
        auto declarator = std::dynamic_pointer_cast<init_deklarator>(children[0]);
        declarator->ntype = ntype;
        declarator->check();
    }
    else if (children.size() == 3) {
        //<lista_init_deklaratora> ::= <lista_init_deklaratora> ZAREZ <init_deklarator>
        auto lista_init_deklaratora_2_child = std::dynamic_pointer_cast<lista_init_deklaratora>(children[0]);
        lista_init_deklaratora_2_child->ntype = ntype;
        lista_init_deklaratora_2_child->check();

        auto init_deklarator_child = std::dynamic_pointer_cast<init_deklarator>(children[2]);
        init_deklarator_child->ntype = ntype;
        init_deklarator_child->check();
    }
};

void init_deklarator::check() {
    if (children.size() == 1) {
        //<init_deklarator> ::= <izravni_deklarator>
        auto declarator = std::dynamic_pointer_cast<izravni_deklarator>(children[0]);
        declarator->ntype = ntype;
        declarator->check();

        auto primitive = std::dynamic_pointer_cast<primitive_type>(declarator->type);
        if (primitive && primitive->is_const)
            error();

        auto arr = std::dynamic_pointer_cast<array_type>(declarator->type);
        if (arr && arr->base_type.is_const)
            error();
    }
    else if (children.size() == 3) {
        //<init_deklarator> ::=  <izravni_deklarator> OP_PRIDRUZI <inicijalizator>
        auto izravni_deklarator_child = std::dynamic_pointer_cast<izravni_deklarator>(children[0]);
        izravni_deklarator_child->ntype = ntype;
        izravni_deklarator_child->check();

        auto inicijalizator_child = std::dynamic_pointer_cast<inicijalizator>(children[2]);
        inicijalizator_child->check();

        if (*izravni_deklarator_child->type == int_t  ||
            *izravni_deklarator_child->type == cint_t ||
            *izravni_deklarator_child->type == char_t ||
            *izravni_deklarator_child->type == cchar_t ){

            if (!inicijalizator_child->type || inicijalizator_child->br_elem != -1)
                error();

            if (!inicijalizator_child->type->is_convertible(primitive_type(std::dynamic_pointer_cast<primitive_type>(izravni_deklarator_child->type)->name, false)))
                error();
        }
        else if(*izravni_deklarator_child->type == array_type(int_t)  ||
                *izravni_deklarator_child->type == array_type(cint_t) ||
                *izravni_deklarator_child->type == array_type(char_t) ||
                *izravni_deklarator_child->type == array_type(cchar_t) ){

            std::shared_ptr<primitive_type> T_type = std::make_shared<primitive_type>(std::dynamic_pointer_cast<array_type>(izravni_deklarator_child->type)->base_type.name, false);

            if (inicijalizator_child->br_elem == -1 || inicijalizator_child->br_elem > izravni_deklarator_child->br_elem)
                error();
            for (auto &it: inicijalizator_child->types) {
                if (!it->is_convertible(*T_type))
                    error();
            }
        }
        else error();
    }
};

void izravni_deklarator::check() {
    if (children.size() == 1) {
        //<izravni_deklarator> ::= IDN
        if (*ntype == void_t)
            error();
        type = ntype;

        auto idn = std::dynamic_pointer_cast<IDN>(children[0]);
        idn->check();
        if (idn->declared_locally)
            error();

        enclosing_scope->declare(idn->value, symbol_entry{type, true});
    }
    else if (children.size() == 4 && typeid(*children[2]) == typeid(BROJ)) {
        // IDN L_UGL_ZAGRADA BROJ D_UGL_ZAGRADA
        if (*ntype == void_t)
            error();

        auto idn = std::dynamic_pointer_cast<IDN>(children[0]);
        idn->check();
        if (idn->declared_locally)
            error();

        auto broj = std::dynamic_pointer_cast<BROJ>(children[2]);
        broj->check();

        if(broj->number <= 0 || broj->number > 1024)
            error();

        type = std::make_shared<array_type>(*std::dynamic_pointer_cast<primitive_type>(ntype));
        br_elem = broj->number;

        enclosing_scope->declare(idn->value, symbol_entry{type, true});
    }
    else if (children.size() == 4 && typeid(*children[2]) == typeid(KR_VOID)) {
        // IDN L_ZAGRADA KR_VOID D_ZAGRADA
        auto idn = std::dynamic_pointer_cast<IDN>(children[0]);
        idn->check();

        type = std::make_shared<function_type>(*std::dynamic_pointer_cast<primitive_type>(ntype), std::vector<std::shared_ptr<ppjc_type>>{});

        if (idn->declared_locally && *idn->type != *type)
            error();

        if (!idn->declared_locally) {
            enclosing_scope->declare(idn->value, symbol_entry{type, false});
            root->declared_functions.emplace_back(idn->value, *std::dynamic_pointer_cast<function_type>(type));
        }
    }
    else if (children.size() == 4 && typeid(*children[2]) == typeid(lista_parametara)) {
        // IDN L_ZAGRADA <lista_parametara> D_ZAGRADA
        auto lista_parametara_child = std::dynamic_pointer_cast<lista_parametara>(children[2]);
        lista_parametara_child->check();

        auto idn = std::dynamic_pointer_cast<IDN>(children[0]);
        idn->check();

        type = std::make_shared<function_type>(*std::dynamic_pointer_cast<primitive_type>(ntype), lista_parametara_child->types);

        if (idn->declared_locally && *idn->type != *type)
            error();

        if (!idn->declared_locally) {
            enclosing_scope->declare(idn->value, symbol_entry{type, false});
            root->declared_functions.emplace_back(idn->value, *std::dynamic_pointer_cast<function_type>(type));
        }
    }
};

void inicijalizator::check() {
    if (children.size() == 1) {
        //<inicijalizator> ::= <izraz_pridruzivanja>
        //std::cout<<"usao u inicijalizator"<<std::endl;
        auto izraz_pridruzivanja_child = std::dynamic_pointer_cast<izraz_pridruzivanja>(children[0]);
        izraz_pridruzivanja_child->check();

        std::shared_ptr<tree_node> tmp_node = children[0];
        while (tmp_node->children.size() == 1) {
            tmp_node = tmp_node->children[0];
        }

        if (typeid(*tmp_node) == typeid(NIZ_ZNAKOVA)) {
            auto niz_znakova_node = std::dynamic_pointer_cast<NIZ_ZNAKOVA>(tmp_node);
            br_elem = niz_znakova_node->parsed.size();
            types.resize(br_elem, std::make_shared<primitive_type>(char_t));
        }
        else {
            type = izraz_pridruzivanja_child->type;
            br_elem = -1;
        }
        //std::cout<<"izasao iz inicijalizatora"<<std::endl;
    }
    else if (children.size() == 3) {
        //<inicijalizator> ::= L_VIT_ZAGRADA <lista_izraza_pridruzivanja> D_VIT_ZAGRADA
        auto lista_izraza_pridruzivanja_child = std::dynamic_pointer_cast<lista_izraza_pridruzivanja>(children[1]);
        lista_izraza_pridruzivanja_child->check();

        types = lista_izraza_pridruzivanja_child->types;
        br_elem = lista_izraza_pridruzivanja_child->br_elem;
    }
};

void lista_izraza_pridruzivanja::check() {
    if(children.size() == 1) {
        //<lista_izraza_pridruzivanja> ::= <izraz_pridruzivanja>
        auto izraz_pridruzivanja_child = std::dynamic_pointer_cast<izraz_pridruzivanja>(children[0]);
        izraz_pridruzivanja_child->check();

        types.clear();

        types.push_back(izraz_pridruzivanja_child->type);
        br_elem = 1;
    }
    else if (children.size() == 3) {
        //<lista_izraza_pridruzivanja> ::= <lista_izraza_pridruzivanja> ZAREZ <izraz_pridruzivanja>
        auto lista_izraza_pridruzivanja_child = std::dynamic_pointer_cast<lista_izraza_pridruzivanja>(children[0]);
        lista_izraza_pridruzivanja_child->check();

        auto izraz_pridruzivanja_child = std::dynamic_pointer_cast<izraz_pridruzivanja>(children[2]);
        izraz_pridruzivanja_child->check();

        copy(lista_izraza_pridruzivanja_child->types.begin(), lista_izraza_pridruzivanja_child->types.end(), back_inserter(types));
        types.push_back(izraz_pridruzivanja_child->type);

        br_elem = lista_izraza_pridruzivanja_child->br_elem + 1;
    }
};

