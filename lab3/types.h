#pragma once
#include <typeinfo>
#include <vector>

struct ppjc_type {
	virtual bool is_convertible(const ppjc_type&) const {
		return false;
	}

	virtual bool is_explicitly_convertible(const ppjc_type& other) const {
		return is_convertible(other);
	}

	virtual bool operator == (const ppjc_type&) const {
		return false;
	}

	bool operator != (const ppjc_type& other) const {
        return !(*this == other);
	}
};

struct primitive_type: ppjc_type {
	enum type_t {VOID, CHAR, INT};
	type_t name;
	bool is_const;

	primitive_type(type_t name, bool is_const = false): name(name), is_const(is_const) {}

	bool is_convertible(const ppjc_type& other) const {
		if (typeid(other) == typeid(primitive_type)) {
			const auto& pother = dynamic_cast<const primitive_type&>(other);
			return name == pother.name || (name == CHAR && pother.name == INT);
		}

		return false;
	}

	bool is_explicitly_convertible(const ppjc_type& other) const {
		if (typeid(other) == typeid(primitive_type)) {
			const auto& pother = dynamic_cast<const primitive_type&>(other);
			return name != VOID && pother.name != VOID;
		}

		return false;
	}

	bool operator == (const ppjc_type& other) const {
		if (typeid(other) == typeid(primitive_type)) {
			const auto& pother = dynamic_cast<const primitive_type&>(other);
			return name == pother.name && is_const == pother.is_const;
		}

		return false;
	}
};

static const primitive_type void_t(primitive_type::VOID), char_t(primitive_type::CHAR), int_t(primitive_type::INT);
static const primitive_type cvoid_t(primitive_type::VOID, true), cchar_t(primitive_type::CHAR, true), cint_t(primitive_type::INT, true);

struct array_type: ppjc_type {
	primitive_type base_type;

	array_type(const primitive_type& base_type): base_type(base_type) {}

	bool is_convertible(const ppjc_type& other) const {
		if (typeid(other) == typeid(array_type)) {
			const auto& aother = dynamic_cast<const array_type&>(other);
			return base_type.name == aother.base_type.name && (!base_type.is_const || aother.base_type.is_const);
		}

		return false;
	}

	bool operator == (const ppjc_type& other) const {
		if (typeid(other) == typeid(array_type)) {
			const auto& aother = dynamic_cast<const array_type&>(other);
			return base_type == aother.base_type;
		}

		return false;
	}
};

struct function_type: ppjc_type {
	primitive_type return_type;
	std::vector<std::shared_ptr<ppjc_type> > parameters;

	function_type(const primitive_type& return_type, const std::vector<std::shared_ptr<ppjc_type>>& parameters):
		return_type(return_type), parameters(parameters) {}

	bool operator == (const ppjc_type& other) const {
		if (typeid(other) == typeid(function_type)) {
			const auto& fother = dynamic_cast<const function_type&>(other);

            //because operator== for vectors of shared_ptr will check by address, not by value
            if (parameters.size() != fother.parameters.size())
                return false;

            for (unsigned int i = 0; i < parameters.size(); i++)
                if (*parameters[i] != *fother.parameters[i])
                    return false;

			return return_type == fother.return_type;
		}

		return false;
	}
};

