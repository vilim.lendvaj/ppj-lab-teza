import pickle
import sys

class eNKA:    
    def __init__(self):
        self.transitions = []
        self.epsilon_transitions = []
        self.size = 0
        self.start = None
        self.finish = None
        self.regex = None

    def add_state(self):
        self.size += 1
        self.transitions.append({})
        self.epsilon_transitions.append([])
        return self.size - 1

    def add_transition(self, origin, destination, alpha):
        self.transitions[origin].setdefault(alpha, []).append(destination)
    
    def add_epsilon_transition(self, origin, destination):
        self.epsilon_transitions[origin].append(destination)

with open('config.bin', 'rb') as f:
    (Stanje, lexer) = pickle.load(f)



#print(lexer["S_pocetno"][0][0].transitions)
#print(lexer["S_pocetno"][0][0].epsilon_transitions)
#print(lexer["S_pocetno"][0][0].start)
#print(lexer["S_pocetno"][0][0].finish)

file = sys.stdin.read()

def epsilon_transition(automat, states):
    stack = [state for state in states]

    while len(stack) > 0:
        parent_state = stack.pop()
        for child_state in automat.epsilon_transitions[parent_state]:
            if child_state not in states:
                stack.append(child_state)
                states.add(child_state)
    return states

def alpha_transition(automat, states, alpha):
    result_state = set()

    for state in states:
        result_state.update(automat.transitions[state].get(alpha, []))
    
    return result_state

def longest_prefix(automat, start_index):
    longest_current = -1
    states = epsilon_transition(automat, {automat.start})
    i = start_index
    cookie = 0
    if automat.finish in states:
        longest_current = 0
    while len(states) > 0 and i < len(file):
        states = alpha_transition(automat, states, file[i])
        states = epsilon_transition(automat, states)
        cookie += 1
        if automat.finish in states:
            longest_current = cookie
        i += 1
    return longest_current

row_count = 1
index = 0
while index < len(file):
    longest = -1
    longest_index = -1
    for i in range(len(lexer[Stanje])):
        res = longest_prefix(lexer[Stanje][i][0], index)
        if(res > longest):
            longest = res
            longest_index = i
    if(longest == -1):
        #print(file[index], file=sys.stderr)
        index += 1
        continue
    
    
    novoStanje = Stanje
    noviRowCount = 0
    for opcija in lexer[Stanje][longest_index][1][1:]:
        if opcija.startswith("VRATI_SE"):
            longest = int(opcija.split(' ')[1])
        if opcija.startswith("UDJI_U_STANJE"):
            novoStanje = opcija.split(' ')[1]
        if opcija.startswith("NOVI_REDAK"):
            noviRowCount += 1
    if(lexer[Stanje][longest_index][1][0] != '-'):
        print(lexer[Stanje][longest_index][1][0], row_count, file[index:index+longest])
    #print("!x!", lexer[Stanje][longest_index][0].regex)
    
    Stanje = novoStanje
    row_count += noviRowCount
    index += longest



#print(epsilon_transition(lexer["S_pocetno"][0][0], {0}))

#print(Stanje)
