import pickle

import sys
import re

from collections import deque 

def je_operator(izraz, i):
            br = 0
            while i-1>=0 and izraz[i-1]=='\\':
                br = br + 1
                i = i - 1
            return br % 2 == 0                            

def pretvori(automat, izraz):
            niz_izbori = []
            br_zagrada = 0
            zadnji_koristen = 0
            for i in range(len(izraz)):
                if izraz[i]=='(' and je_operator(izraz, i):
                    br_zagrada = br_zagrada + 1
                elif izraz[i] == ')' and je_operator(izraz, i):
                    br_zagrada = br_zagrada - 1
                elif br_zagrada == 0 and izraz[i] == '|' and je_operator(izraz, i):
                    niz_izbori.append(izraz[zadnji_koristen:i])
                    zadnji_koristen = i + 1
            if len(niz_izbori) > 0:
                niz_izbori.append(izraz[zadnji_koristen:])
            
            lijevo_stanje = automat.add_state()
            desno_stanje = automat.add_state()

            if len(niz_izbori) > 1:    #ako je pronaden jedan operator izbora
                for i in range(len(niz_izbori)):
                    (privremeno_lijevo, privremeno_desno) = pretvori(automat, niz_izbori[i])
                    automat.add_epsilon_transition(lijevo_stanje, privremeno_lijevo)
                    automat.add_epsilon_transition(privremeno_desno, desno_stanje)
            else:
                prefiksirano = False
                zadnje_stanje = lijevo_stanje
                i = 0
                while i < len(izraz):
                    a, b = None, None
                    if prefiksirano:
                        prefiksirano = False
                        prijelazni_znak = None
                        if izraz[i] == 't':
                            prijelazni_znak = '\t'
                        elif izraz[i] == 'n':
                            prijelazni_znak = '\n'
                        elif izraz[i] == '_':
                            prijelazni_znak = ' '
                        else:
                            prijelazni_znak = izraz[i]

                        a = automat.add_state()
                        b = automat.add_state()
                        
                        automat.add_transition(a, b, prijelazni_znak)
                    else:
                        if izraz[i] == '\\':
                            prefiksirano = True
                            i += 1
                            continue

                        if izraz[i] != '(':

                            a = automat.add_state()
                            b = automat.add_state()

                            if izraz[i] == '$':
                                automat.add_epsilon_transition(a, b)
                            else:
                                automat.add_transition(a, b, izraz[i])
                        
                        else:
                            bracket_cookie = 0
                            bracket_end = None
                            for j in range(i+1, len(izraz)):
                                if izraz[j] == '(':
                                    bracket_cookie += 1
                                elif izraz[j] == ')' and bracket_cookie != 0:
                                    bracket_cookie -= 1
                                elif izraz[j] == ')' and bracket_cookie == 0:
                                    bracket_end = j
                                    break
                            (a, b) = pretvori(automat, izraz[i+1:bracket_end])
                            i = bracket_end
                        

                    if i + 1 < len(izraz) and izraz[i + 1] == '*':
                        x = a
                        y = b

                        a = automat.add_state()
                        b = automat.add_state()

                        automat.add_epsilon_transition(a, x)
                        automat.add_epsilon_transition(y, b)
                        automat.add_epsilon_transition(a, b)
                        automat.add_epsilon_transition(y, x)
                        i += 1
                
                    automat.add_epsilon_transition(zadnje_stanje, a)
                    zadnje_stanje = b
                    i += 1
                automat.add_epsilon_transition(zadnje_stanje, desno_stanje)
            return (lijevo_stanje, desno_stanje)


class eNKA:    
    def __init__(self):
        self.transitions = []
        self.epsilon_transitions = []
        self.size = 0
        self.start = None
        self.finish = None
        self.regex = None

    def add_state(self):
        self.size += 1
        self.transitions.append({})
        self.epsilon_transitions.append([])
        return self.size - 1

    def add_transition(self, origin, destination, alpha):
        self.transitions[origin].setdefault(alpha, []).append(destination)
    
    def add_epsilon_transition(self, origin, destination):
        self.epsilon_transitions[origin].append(destination)


regex_expressions = {}

def replace_regex_subdefinition(regex):
    for key, value in regex_expressions.items():
        i = regex.find(key, 0)
        while i != -1:
            if(je_operator(regex, i) and je_operator(regex, i + len(key) - 1)):
                regex = regex[:i] + '(' + value + ')' + regex[i + len(key):]
            i = regex.find(key, i+len(key))
    return regex

def main():
    
    #read all lines from stdin
    input_lines = sys.stdin.readlines()

    #remove right \r\n
    input_lines = [line.rstrip() for line in input_lines]
    
    regex_pattern = re.compile("(\{[a-zA-Z]+\}) (.+)")

    #iterate regex definitions
    index = 0
    while index < len(input_lines):
        res = regex_pattern.search(input_lines[index])
        if res:
            expression_key = res.group(1)
            expression = res.group(2)
            regex_expressions[expression_key] = replace_regex_subdefinition(expression)
        else:
            break      # finished with parsing regex definitions
        index+=1
    
    if input_lines[index].startswith("%X"):
        lex_states = input_lines[index].split(' ')[1:]
        index+=1

    if input_lines[index].startswith("%L"):
        lex_identifiers = input_lines[index].split(' ')[1:]
        index+=1

    states_pattern = re.compile("<(S_[a-zA-Z]+)>(\S+)\n\{\n((?:[^\}]*\n)+)\}")

    states = states_pattern.findall("\n".join(input_lines[index:]))

    lexer = {}

    for state in states:
        e = eNKA()
        (start, finish) = pretvori(e, replace_regex_subdefinition(state[1]))
        e.start, e.finish = start, finish
        e.regex = replace_regex_subdefinition(state[1])
        lexer.setdefault(state[0], []).append((e, state[2].split('\n')[:-1]))
        #print("Stanje:", state[0], "Regex: ", e, "Opcije:", state[2].split('\n')[:-1])
    
    #print(lexer)
    with open('analizator/config.bin', 'wb') as f:
        pickle.dump((lex_states[0], lexer), f, protocol=4)


if __name__ == "__main__":
    main()
