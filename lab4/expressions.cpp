#include "terminals.h"
#include "expressions.h"
#include "statements.h"
#include "decl_def.h"
#include "types.h"


void primarni_izraz::check() {
    if (children.size() == 3) {
        //<primarni_izraz> ::= L_ZAGRADA <izraz> D_ZAGRADA
        auto child = std::dynamic_pointer_cast<izraz>(children[1]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        //noop, forward top of stack
    } else {
        if (typeid(*children[0]) == typeid(BROJ)) {
            //<primarni_izraz> ::= BROJ
            auto child = std::dynamic_pointer_cast<BROJ>(children[0]);
            child->check();

            if (!child->is_valid)
                error();

            type = child->type;
            lvalue = false;

            if (child->number == ((child->number << 12) >> 12)) {
                root->fout<<"\t\tMOVE %D "<<child->number<<", R0\n";
            } else {
                root->fout<<"\t\tMOVE %D "<<(child->number >> 16)<<", R0\n";
                root->fout<<"\t\tSHL R0, %D 16, R0\n";
                root->fout<<"\t\tOR R0, %D "<<(child->number & 0xffff)<<", R0\n";
            }
            root->fout<<"\t\tPUSH R0\n";
        }
        else if (typeid(*children[0]) == typeid(ZNAK)) {
            //<primarni_izraz> ::= ZNAK
            auto child = std::dynamic_pointer_cast<ZNAK>(children[0]);
            child->check();

            if (!child->is_valid)
                error();

            type = child->type;
            lvalue = false;

            root->fout<<"\t\tMOVE %D "<<child->number<<", R0\n";
            root->fout<<"\t\tPUSH R0\n";
        }
        else if (typeid(*children[0]) == typeid(NIZ_ZNAKOVA)) {
            //<primarni_izraz> ::= NIZ_ZNAKOVA
            auto child = std::dynamic_pointer_cast<NIZ_ZNAKOVA>(children[0]);
            child->check();

            if (!child->is_valid)
                error();

            type = child->type;
            lvalue = false;

            root->fout<<"\t\tJR SKIP_"<<id<<"\n";
            root->fout<<"S_"<<id<<"\tDW";
            for (char c: child->parsed) {
                root->fout<<" %D "<<(int) c<<",";
            }
            root->fout<<"\n";

            root->fout<<"SKIP_"<<id<<"\tMOVE S_"<<id<<", R0\n";
            root->fout<<"\t\tPUSH R0\n";
        }
        else {
            //<primarni_izraz> ::= IDN
            auto child = std::dynamic_pointer_cast<IDN>(children[0]);
            child->check();

            if (!child->entry)
                error();

            type = child->entry->type;
            lvalue = child->lvalue;
            entry = child->entry;
            value = child->value;

            if (typeid(*type) == typeid(primitive_type)) {
                std::shared_ptr<primitive_type> ptype = std::dynamic_pointer_cast<primitive_type>(type);
                if (entry->global) {
                    root->fout<<"\t\tLOAD R0, (G_"<<child->value<<")\n";
                    root->fout<<"\t\tPUSH R0\n";
                } else {
                    root->fout<<"\t\tLOAD R0, (R6+%D "<<entry->offset<<")\n";
                    root->fout<<"\t\tPUSH R0\n";
                }
            } else if (typeid(*type) == typeid(array_type)) {
                std::shared_ptr<array_type> atype = std::dynamic_pointer_cast<array_type>(type);
                if (entry->global) {
                    root->fout<<"\t\tMOVE G_"<<child->value<<", R0\n";
                    root->fout<<"\t\tPUSH R0\n";
                } else {
                    root->fout<<"\t\tLOAD R0, (R6+%D "<<entry->offset<<")\n";
                    root->fout<<"\t\tPUSH R0\n";
                }
            }
            //handle functions upstream
        }
    }
}

void postfiks_izraz::check() {
    if (children.size() == 1) {
        //<postfiks_izraz> ::= <primarni_izraz>
        auto child = std::dynamic_pointer_cast<primarni_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        //noop, forward top of stack
    }
    else if (children.size() == 4 && children[2]->str == "<izraz>") {
        //<postfiks_izraz> ::= <postfiks_izraz> L_UGL_ZAGRADA <izraz> D_UGL_ZAGRADA
        auto postfiks_izraz_child = std::dynamic_pointer_cast<postfiks_izraz>(children[0]);
        postfiks_izraz_child->check();

        if (*postfiks_izraz_child->type != array_type(int_t) &&
           *postfiks_izraz_child->type != array_type(char_t) &&
           *postfiks_izraz_child->type != array_type(cint_t) &&
           *postfiks_izraz_child->type != array_type(cchar_t) )
            error();

        auto izraz_child = std::dynamic_pointer_cast<izraz>(children[2]);
        izraz_child->check();

        if (!izraz_child->type->is_convertible(int_t))
            error();

        auto postfiks_izraz_child_type = std::dynamic_pointer_cast<array_type>(postfiks_izraz_child->type);
        type = std::make_shared<primitive_type>(postfiks_izraz_child_type->base_type);

        lvalue = !postfiks_izraz_child_type->base_type.is_const;
        is_array_indexing = true;

        //noop, forward top of stack
    }
    else if (children.size() == 3) {
        //<postfiks_izraz> ::= <postfiks_izraz> L_ZAGRADA D_ZAGRADA
        auto postfiks_izraz_child = std::dynamic_pointer_cast<postfiks_izraz>(children[0]);
        postfiks_izraz_child->check();

        if (typeid(*postfiks_izraz_child->type) != typeid(function_type))
            error();

        auto postfiks_izraz_child_type = std::dynamic_pointer_cast<function_type>(postfiks_izraz_child->type);

        if (postfiks_izraz_child_type->parameters.size() != 0)
            error();

        type = std::make_shared<primitive_type>(postfiks_izraz_child_type->return_type);
        lvalue = false;

        root->fout<<"\t\tPUSH R6\n";
        root->fout<<"\t\tCALL F_"<<postfiks_izraz_child->value<<"\n";

        if (*type == void_t) {
            root->fout<<"\t\tPOP R6\n";
        } else {
            root->fout<<"\t\tMOVE R6, R0\n";
            root->fout<<"\t\tPOP R6\n";
            root->fout<<"\t\tPUSH R0\n";
        }
    }
    else if (children.size() == 4 && children[2]->str == "<lista_argumenata>") {
        //<postfiks_izraz> ::= <postfiks_izraz> L_ZAGRADA <lista_argumenata> D_ZAGRADA
        auto postfiks_izraz_child = std::dynamic_pointer_cast<postfiks_izraz>(children[0]);
        postfiks_izraz_child->check();

        root->fout<<"\t\tPUSH R6\n";

        auto lista_argumenata_child = std::dynamic_pointer_cast<lista_argumenata>(children[2]);
        lista_argumenata_child->check();

        if (typeid(*postfiks_izraz_child->type) != typeid(function_type))
            error();

        auto postfiks_izraz_child_type = std::dynamic_pointer_cast<function_type>(postfiks_izraz_child->type);

        if (postfiks_izraz_child_type->parameters.size() != lista_argumenata_child->types.size())
            error();

        for (unsigned int i = 0; i < lista_argumenata_child->types.size(); i++) {
            if (!lista_argumenata_child->types[i]->is_convertible(*postfiks_izraz_child_type->parameters[i]))
                error();
        }

        type = std::make_shared<primitive_type>(postfiks_izraz_child_type->return_type);
        lvalue = false;

        root->fout<<"\t\tCALL F_"<<postfiks_izraz_child->value<<"\n";

        root->fout<<"\t\tADD R7, %D "<<4 * postfiks_izraz_child_type->parameters.size()<<", R7\n";

        if (*type == void_t) {
            root->fout<<"\t\tPOP R6\n";
        } else {
            root->fout<<"\t\tMOVE R6, R0\n";
            root->fout<<"\t\tPOP R6\n";
            root->fout<<"\t\tPUSH R0\n";
        }
    }
    else if (children.size() == 2) {
        //<postfiks_izraz> ::= <postfiks_izraz> (OP_INC | OP_DEC)
        auto postfix_izraz_child = std::dynamic_pointer_cast<postfiks_izraz>(children[0]);
        postfix_izraz_child->check();

        if (!postfix_izraz_child->lvalue || !postfix_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;

        root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tPUSH R0\n";

        auto operator_child = std::dynamic_pointer_cast<terminal>(children[1]);

        if (operator_child->name == "OP_INC") {  //promjena, str sadrzi dodatne stvari uz OP_INC
            root->fout<<"\t\tADD R0, 1, R0\n";
        } else {
            root->fout<<"\t\tSUB R0, 1, R0\n";
        }

        if (postfix_izraz_child->entry->global) {
            root->fout<<"\t\tSTORE R0, (G_"<<postfix_izraz_child->value<<")\n";
        } else {
            root->fout<<"\t\tSTORE R0, (R6+%D "<<postfix_izraz_child->entry->offset<<")\n";
        }
    }
}

void lista_argumenata::check() {
    if (children.size() == 1) {
        //<lista_argumenata> ::= <izraz_pridruzivanja>
        auto izraz_pridruzivanja_child = std::dynamic_pointer_cast<izraz_pridruzivanja>(children[0]);
        izraz_pridruzivanja_child->check();

        types.push_back(izraz_pridruzivanja_child->type);
    }
    else if (children.size() == 3) {
        //<lista_argumenata> ::= <lista_argumenata> ZAREZ <izraz_pridruzivanja>
        auto lista_argumenata_child = std::dynamic_pointer_cast<lista_argumenata>(children[0]);
        lista_argumenata_child->check();

        auto izraz_pridruzivanja_child = std::dynamic_pointer_cast<izraz_pridruzivanja>(children[2]);
        izraz_pridruzivanja_child->check();

        copy(lista_argumenata_child->types.begin(), lista_argumenata_child->types.end(), back_inserter(types));

        types.push_back(izraz_pridruzivanja_child->type);
    }
}

void unarni_izraz::check() {
    if (children.size() == 1) {
        //<unarni_izraz> ::= <postfiks_izraz>
        auto child = std::dynamic_pointer_cast<postfiks_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        if (child->is_array_indexing) {
            //*(ptr+4*idx)
            root->fout<<"\t\tPOP R1\n";
            root->fout<<"\t\tPOP R0\n";
            root->fout<<"\t\tSHL R1, 2, R1\n";
            root->fout<<"\t\tADD R0, R1, R0\n";
            root->fout<<"\t\tLOAD R0, (R0)\n";
            root->fout<<"\t\tPUSH R0\n";
        } // else noop, forward top of stack
    }
    else if (children.size() == 2 && children[1]->str == "<unarni_izraz>") {
        //(<unarni_izraz> ::= (OP_INC | OP_DEC) <unarni_izraz>
        auto unarni_izraz_child = std::dynamic_pointer_cast<unarni_izraz>(children[1]);
        unarni_izraz_child->check();

        if(unarni_izraz_child->lvalue == false || !unarni_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;

        root->fout<<"\t\tPOP R0\n";

        auto operator_child = std::dynamic_pointer_cast<terminal>(children[0]);

        if (operator_child->name == "OP_INC") {  //promjena, str sadrzi dodatne stvari uz OP_INC
            root->fout<<"\t\tADD R0, 1, R0\n";
        } else {
            root->fout<<"\t\tSUB R0, 1, R0\n";
        }

        root->fout<<"\t\tPUSH R0\n";

        if (unarni_izraz_child->entry->global) {
            root->fout<<"\t\tSTORE R0, (G_"<<unarni_izraz_child->value<<")\n";
        } else {
            root->fout<<"\t\tSTORE R0, (R6+%D "<<unarni_izraz_child->entry->offset<<")\n";
        }
    }
    else if (children.size() == 2 && children[0]->str == "<unarni_operator>") {
        //<unarni_izraz> ::= <unarni_operator> <cast_izraz>
        auto cast_izraz_child = std::dynamic_pointer_cast<cast_izraz>(children[1]);
        cast_izraz_child->check();

        if (!cast_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;

        auto op = std::dynamic_pointer_cast<unarni_operator>(children[0]);
        op->check();

        if (op->name == "OP_TILDA") {
            root->fout<<"\t\tPOP R0\n";
            root->fout<<"\t\tXOR R0, -1, R0\n";
            root->fout<<"\t\tPUSH R0\n";
        } else if (op->name == "OP_NEG") {
            root->fout<<"\t\tPOP R0\n";
            root->fout<<"\t\tOR R0, R0, R0\n";
            root->fout<<"\t\tMOVE SR, R0\n";
            root->fout<<"\t\tSHR R0, 3, R0\n"; //zastavica Z
            root->fout<<"\t\tAND R0, 1, R0\n";
            root->fout<<"\t\tPUSH R0\n";
        } else if (op->name == "PLUS") {
            //noop
        } else {
            root->fout<<"\t\tPOP R0\n";
            root->fout<<"\t\tMOVE 0, R1\n";
            root->fout<<"\t\tSUB R1, R0, R0\n";
            root->fout<<"\t\tPUSH R0\n";
        }
    }
}

void unarni_operator::check() {
    name = std::dynamic_pointer_cast<terminal>(children[0])->name;
}

void cast_izraz::check() {
    if (children.size() == 1) {
        //<cast_izraz> ::= <unarni_izraz>
        auto child = std::dynamic_pointer_cast<unarni_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        //noop, forward top of stack
    }
    else if (children.size() == 4) {
        //<cast_izraz> ::= L_ZAGRADA <ime_tipa> D_ZAGRADA <cast_izraz>
        auto ime_tipa_child = std::dynamic_pointer_cast<ime_tipa>(children[1]);
        ime_tipa_child->check();

        auto cast_izraz_child = std::dynamic_pointer_cast<cast_izraz>(children[3]);
        cast_izraz_child->check();

        if (!cast_izraz_child->type->is_explicitly_convertible(*ime_tipa_child->type))
            error();

        type = ime_tipa_child->type;
        lvalue = false;

        //noop, forward top of stack
        //mogli bismo andati int s 0xff da dobijemo char, ali za potrebe ove vjezbe nije potrebno
    }
}

void ime_tipa::check() {
    if (children.size() == 1){
        //<ime_tipa> ::= <specifikator_tipa>
        auto specifikator_tipa_ptr = std::dynamic_pointer_cast<specifikator_tipa>(children[0]);
        specifikator_tipa_ptr->check();
        type = specifikator_tipa_ptr->type;
    }
    else if (children.size() == 2){
        //<ime_tipa> ::= KR_CONST <specifikator_tipa>
        auto specifikator_tipa_ptr = std::dynamic_pointer_cast<specifikator_tipa>(children[1]);
        specifikator_tipa_ptr->check();
        if (*specifikator_tipa_ptr->type == void_t)
            error();

        type = std::make_shared<primitive_type>(specifikator_tipa_ptr->type->name, true);     //convert type to const
    }
}

void specifikator_tipa::check() {
    children[0]->check();

    //<specifikator_tipa> ::= KR_VOID
    if (typeid(*children[0]) == typeid(KR_VOID))
        type = std::dynamic_pointer_cast<KR_VOID>(children[0])->type;

	//<specifikator_tipa> ::= KR_CHAR
    else if(typeid(*children[0]) == typeid(KR_CHAR))
        type = std::dynamic_pointer_cast<KR_CHAR>(children[0])->type;

	//<specifikator_tipa> ::= KR_INT
    else if(typeid(*children[0]) == typeid(KR_INT))
        type = std::dynamic_pointer_cast<KR_INT>(children[0])->type;
}

void multiplikativni_izraz::check() {
    if (children.size() == 1) {
        //<multiplikativni_izraz> ::= <cast_izraz>
        auto child = std::dynamic_pointer_cast<cast_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        //noop, forward top of stack
    }
    else if (children.size() == 3) {
        //<multiplikativni_izraz> ::= <multiplikativni_izraz> (OP_PUTA | OP_DIJELI | OP_MOD) <cast_izraz>
        root->fout<<"\t\tPUSH R6\n";

        auto multiplikativni_izraz_child = std::dynamic_pointer_cast<multiplikativni_izraz>(children[0]);
        multiplikativni_izraz_child->check();

        if(!multiplikativni_izraz_child->type->is_convertible(int_t))
            error();

        auto cast_izraz_child = std::dynamic_pointer_cast<cast_izraz>(children[2]);
        cast_izraz_child->check();

        if(!cast_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;

        auto op = std::dynamic_pointer_cast<terminal>(children[1]);

        if (op->name == "OP_PUTA") {
            root->fout<<"\t\tCALL OP_MUL\n";
        } else if (op->name == "OP_DIJELI") {
            root->fout<<"\t\tCALL OP_DIV\n";
        } else {
            root->fout<<"\t\tCALL OP_MOD\n";
        }
        root->fout<<"\t\tADD R7, 8, R7\n";
        root->fout<<"\t\tMOVE R6, R0\n";
        root->fout<<"\t\tPOP R6\n";
        root->fout<<"\t\tPUSH R0\n";
    }
}

void aditivni_izraz::check() {
    if (children.size() == 1) {
        //<aditivni_izraz> ::= <multiplikativni_izraz>
        auto child = std::dynamic_pointer_cast<multiplikativni_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        //noop, forward top of stack
    }
    else if (children.size() == 3) {
        //<aditivni_izraz> ::= <aditivni_izraz> (PLUS | MINUS) <multiplikativni_izraz>

        auto aditivni_izraz_child = std::dynamic_pointer_cast<aditivni_izraz>(children[0]);
        aditivni_izraz_child->check();

        if (!aditivni_izraz_child->type->is_convertible(int_t))
            error();

        auto multiplikativni_izraz_child = std::dynamic_pointer_cast<multiplikativni_izraz>(children[2]);
        multiplikativni_izraz_child->check();

        if (!multiplikativni_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;

        auto op = std::dynamic_pointer_cast<terminal>(children[1]);

        root->fout<<"\t\tPOP R1\n";
        root->fout<<"\t\tPOP R0\n";
        if (op->name == "PLUS") {
            root->fout<<"\t\tADD R0, R1, R0\n";
        } else {
            root->fout<<"\t\tSUB R0, R1, R0\n";
        }
        root->fout<<"\t\tPUSH R0\n";
    }
}

void odnosni_izraz::check() {
    if (children.size() == 1) {
        //<odnosni_izraz> ::= <aditivni_izraz>
        auto child = std::dynamic_pointer_cast<aditivni_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        //noop, forward top of stack
    }
    else if (children.size() == 3){
        //<odnosni_izraz> ::= <odnosni_izraz> (OP_LT | OP_GT | OP_LTE | OP_GTE) <aditivni_izraz>

        auto odnosni_izraz_child = std::dynamic_pointer_cast<odnosni_izraz>(children[0]);
        odnosni_izraz_child->check();

        if (!odnosni_izraz_child->type->is_convertible(int_t))
            error();

        auto aditivni_izraz_child = std::dynamic_pointer_cast<aditivni_izraz>(children[2]);
        aditivni_izraz_child->check();

        if (!aditivni_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;

        auto op = std::dynamic_pointer_cast<terminal>(children[1]);

        root->fout<<"\t\tPOP R1\n";
        root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tCMP R0, R1\n";

        if (op->name == "OP_LT") {
            root->fout<<"\t\tJR_SLT TRUE_"<<id<<"\n";       //dodano S signed operator
        } else if (op->name == "OP_GT") {
            root->fout<<"\t\tJR_SGT TRUE_"<<id<<"\n";
        } else if (op->name == "OP_LTE") {
            root->fout<<"\t\tJR_SLE TRUE_"<<id<<"\n";
        } else {
            root->fout<<"\t\tJR_SGE TRUE_"<<id<<"\n";
        }
        root->fout<<"\t\tMOVE 0, R0\n";
        root->fout<<"\t\tJR END_"<<id<<"\n";
        root->fout<<"TRUE_"<<id<<"\t\tMOVE 1, R0\n";
        root->fout<<"END_"<<id<<"\t\tPUSH R0\n";
    }
}

void jednakosni_izraz::check() {
    if (children.size() == 1) {
        //<jednakosni_izraz> ::= <odnosni_izraz>
        auto child = std::dynamic_pointer_cast<odnosni_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        //noop, forward top of stack
    }
    else if (children.size() == 3){
        //<jednakosni_izraz> ::= <jednakosni_izraz> (OP_EQ | OP_NEQ) <odnosni_izraz>
        auto jednakosni_izraz_child = std::dynamic_pointer_cast<jednakosni_izraz>(children[0]);
        jednakosni_izraz_child->check();

        if(!jednakosni_izraz_child->type->is_convertible(int_t))
            error();

        auto odnosni_izraz_child = std::dynamic_pointer_cast<odnosni_izraz>(children[2]);
        odnosni_izraz_child->check();

        if(!odnosni_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;

        auto op = std::dynamic_pointer_cast<terminal>(children[1]);

        root->fout<<"\t\tPOP R1\n";
        root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tCMP R0, R1\n";

        if (op->name == "OP_EQ") {
            root->fout<<"\t\tJR_EQ TRUE_"<<id<<"\n";
        } else {
            root->fout<<"\t\tJR_NE TRUE_"<<id<<"\n";
        }
        root->fout<<"\t\tMOVE 0, R0\n";
        root->fout<<"\t\tJR END_"<<id<<"\n";
        root->fout<<"TRUE_"<<id<<"\tMOVE 1, R0\n";
        root->fout<<"END_"<<id<<"\tPUSH R0\n";
    }
}

void bin_i_izraz::check() {
    if (children.size() == 1) {
        //<bin_i_izraz> ::= <jednakosni_izraz>
        auto child = std::dynamic_pointer_cast<jednakosni_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        //noop, forward top of stack
    }
    else if (children.size() == 3) {
        //<bin_i_izraz> ::= <bin_i_izraz> OP_BIN_I <jednakosni_izraz>
        auto bin_i_izraz_child = std::dynamic_pointer_cast<bin_i_izraz>(children[0]);
        bin_i_izraz_child->check();

        if(!bin_i_izraz_child->type->is_convertible(int_t))
            error();

        auto jednakosni_izraz_child = std::dynamic_pointer_cast<jednakosni_izraz>(children[2]);
        jednakosni_izraz_child->check();

        if(!jednakosni_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;

        root->fout<<"\t\tPOP R1\n";
        root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tAND R0, R1, R0\n";
        root->fout<<"\t\tPUSH R0\n";
    }
}

void bin_xili_izraz::check() {
    if (children.size() == 1) {
        //<bin_xili_izraz> ::= <bin_i_izraz>
        auto child = std::dynamic_pointer_cast<bin_i_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        //noop, forward top of stack
    }
    else if (children.size() == 3) {
        //<bin_xili_izraz> ::= <bin_xili_izraz> OP_BIN_XILI <bin_i_izraz>
        auto bin_xili_izraz_child = std::dynamic_pointer_cast<bin_xili_izraz>(children[0]);
        bin_xili_izraz_child->check();

        if(!bin_xili_izraz_child->type->is_convertible(int_t))
            error();

        auto bin_i_izraz_child = std::dynamic_pointer_cast<bin_i_izraz>(children[2]);
        bin_i_izraz_child->check();

        if(!bin_i_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;

        root->fout<<"\t\tPOP R1\n";
        root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tXOR R0, R1, R0\n";
        root->fout<<"\t\tPUSH R0\n";
    }
}

void bin_ili_izraz::check() {
    if (children.size() == 1) {
        //<bin_ili_izraz> ::= <bin_xili_izraz>
        auto child = std::dynamic_pointer_cast<bin_xili_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        //noop, forward top of stack
    }
    else if (children.size() == 3) {
        //<bin_ili_izraz> ::= <bin_ili_izraz> OP_BIN_ILI <bin_xili_izraz>
        auto bin_ili_izraz_child = std::dynamic_pointer_cast<bin_ili_izraz>(children[0]);
        bin_ili_izraz_child->check();

        if(!bin_ili_izraz_child->type->is_convertible(int_t))
            error();

        auto bin_xili_izraz_child = std::dynamic_pointer_cast<bin_xili_izraz>(children[2]);
        bin_xili_izraz_child->check();

        if(!bin_xili_izraz_child->type->is_convertible(int_t))
            error();

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;

        root->fout<<"\t\tPOP R1\n";
        root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tOR R0, R1, R0\n";
        root->fout<<"\t\tPUSH R0\n";
    }
}

void log_i_izraz::check() {
    if (children.size() == 1) {
        //<log_i_izraz> ::= <bin_ili_izraz>
        auto child = std::dynamic_pointer_cast<bin_ili_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        //noop, forward top of stack
    }
    else if (children.size() == 3) {
        //<log_i_izraz> ::= <log_i_izraz> OP_I <bin_ili_izraz>
        auto log_ili_izraz_child = std::dynamic_pointer_cast<log_i_izraz>(children[0]);
        log_ili_izraz_child->check();

        if(!log_ili_izraz_child->type->is_convertible(int_t))
            error();

        root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tOR R0, R0, R0\n";
        root->fout<<"\t\tJR_Z FALSE_"<<id<<"\n";

        auto bin_ili_izraz_child = std::dynamic_pointer_cast<bin_ili_izraz>(children[2]);
        bin_ili_izraz_child->check();

        if(!bin_ili_izraz_child->type->is_convertible(int_t))
            error();

        root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tOR R0, R0, R0\n";
        root->fout<<"\t\tJR_Z FALSE_"<<id<<"\n";

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;

        root->fout<<"\t\tMOVE 1, R0\n";
        root->fout<<"\t\tJR END_"<<id<<"\n";
        root->fout<<"FALSE_"<<id<<"\tMOVE 0, R0\n";
        root->fout<<"END_"<<id<<"\tPUSH R0\n";
    }
}

void log_ili_izraz::check() {
    if (children.size() == 1) {
        //<log_ili_izraz> ::= <log_i_izraz>
        auto child = std::dynamic_pointer_cast<log_i_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        //noop, forward top of stack
    }
    else if (children.size() == 3) {
        //<log_ili_izraz> ::= <log_ili_izraz> OP_ILI <log_i_izraz>
        auto log_ili_izraz_child = std::dynamic_pointer_cast<log_ili_izraz>(children[0]);
        log_ili_izraz_child->check();

        if(!log_ili_izraz_child->type->is_convertible(int_t))
            error();

        root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tOR R0, R0, R0\n";
        root->fout<<"\t\tJR_NZ TRUE_"<<id<<"\n";

        auto log_i_izraz_child = std::dynamic_pointer_cast<log_i_izraz>(children[2]);
        log_i_izraz_child->check();

        if(!log_i_izraz_child->type->is_convertible(int_t))
            error();

        root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tOR R0, R0, R0\n";
        root->fout<<"\t\tJR_NZ TRUE_"<<id<<"\n";

        type = std::make_shared<primitive_type>(int_t);
        lvalue = false;

        root->fout<<"\t\tMOVE 0, R0\n";
        root->fout<<"\t\tJR END_"<<id<<"\n";
        root->fout<<"TRUE_"<<id<<"\tMOVE 1, R0\n";
        root->fout<<"END_"<<id<<"\tPUSH R0\n";
    }
}

void izraz_pridruzivanja::check() {
    if (children.size() == 1) {
        //<izraz_pridruzivanja> ::= <log_ili_izraz>
        auto child = std::dynamic_pointer_cast<log_ili_izraz>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        //noop, forward top of stack
    }
    else if (children.size() == 3) {
        //<izraz_pridruzivanja> ::= <postfiks_izraz> OP_PRIDRUZI <izraz_pridruzivanja>
        auto postfiks_izraz_child = std::dynamic_pointer_cast<postfiks_izraz>(children[0]);
        postfiks_izraz_child->check();

        if (!postfiks_izraz_child->lvalue)
            error();

        auto izraz_pridruzivanja_child = std::dynamic_pointer_cast<izraz_pridruzivanja>(children[2]);
        izraz_pridruzivanja_child->check();

        if (!izraz_pridruzivanja_child->type->is_convertible(*postfiks_izraz_child->type))
            error();

        type = postfiks_izraz_child->type;
        lvalue = false;

        if (postfiks_izraz_child->is_array_indexing) {
            root->fout<<"\t\tPOP R2\n";
            root->fout<<"\t\tPOP R1\n";
            root->fout<<"\t\tPOP R0\n";
            root->fout<<"\t\tSHL R1, 2, R1\n";
            root->fout<<"\t\tADD R0, R1, R0\n";
            root->fout<<"\t\tSTORE R2, (R0)\n";
            root->fout<<"\t\tPUSH R2\n";
        } else {
            root->fout<<"\t\tPOP R1\n";
            root->fout<<"\t\tPOP R0\n";
            if (postfiks_izraz_child->entry->global) {
                root->fout<<"\t\tSTORE R1, (G_"<<postfiks_izraz_child->value<<")\n";
            } else {
                root->fout<<"\t\tSTORE R1, (R6+%D "<<postfiks_izraz_child->entry->offset<<")\n";
            }
            root->fout<<"\t\tPUSH R1\n";
        }
    }
}

void izraz::check() {
    if (children.size() == 1) {
        //<izraz> ::= <izraz_pridruzivanja>
        auto child = std::dynamic_pointer_cast<izraz_pridruzivanja>(children[0]);
        child->check();
        type = child->type;
        lvalue = child->lvalue;
        entry = child->entry;
        value = child->value;

        //noop, forward top of stack
    }
    else if (children.size() == 3) {
        //<izraz> ::= <izraz> ZAREZ <izraz_pridruzivanja>
        auto izraz_child = std::dynamic_pointer_cast<izraz>(children[0]);
        izraz_child->check();

        if (*izraz_child->type != void_t) {
            root->fout<<"\t\tPOP R0\n"; //discard result
        }

        auto izraz_pridruzivanja_child = std::dynamic_pointer_cast<izraz_pridruzivanja>(children[2]);
        izraz_pridruzivanja_child->check();

        type = izraz_pridruzivanja_child->type;
        lvalue = false;

        //noop, forward top of stack
    }
}

