#pragma once
#include <sstream>
#include "tree.h"

struct terminal: tree_node {
    std::string name;
    int line;
    std::string value;

    void init() override {
        std::istringstream sin(str);
        sin>>name>>line;
        sin.get(); // razmak
        std::getline(sin, value);
    }

    std::string to_string() override {
        std::ostringstream sout;
        sout<<name<<'('<<line<<','<<value<<')';
        return sout.str();
    }
};

struct IDN: terminal {
	bool declared_locally = false;
	bool lvalue;
	symbol_entry *entry;

	void check() {
        entry = enclosing_scope->find(value);
        if (entry) {
            declared_locally = enclosing_scope->is_local(value);
            lvalue = (*entry->type == int_t || *entry->type == char_t);
        }
	}
};

struct BROJ: terminal {
	int number;
	std::shared_ptr<ppjc_type> type;
	bool is_valid;

	void check() {
        try {
            number = std::stoi(value, 0, 0);
        }
        catch (...) {
            is_valid = false;
            return;
        }
        type = std::make_shared<primitive_type>(int_t);
        is_valid = true;
	}
};

struct ZNAK: terminal {
	std::shared_ptr<ppjc_type> type;
	bool is_valid;
	int number;

	void check() {
	    //std::cout<<value<<' '<<"'\\0'"<<' '<<(value != "'\\0'")<<std::endl;
        type = std::make_shared<primitive_type>(char_t);
        is_valid = true;

        if (value.size() > 3) {
            if (value.size() == 4 && value[1] == '\\') {
                switch (value[2]) {
                case 't':
                    number = '\t';
                    return;
                case 'n':
                    number = '\n';
                    return;
                case '0':
                    number = 0;
                    return;
                case '"':
                    number = '"';
                    return;
                case '\'':
                    number = '\"';
                    return;
                case '\\':
                    number = '\\';
                    return;
                }
            }
            is_valid = false;
            return;
        }
        if (value.size() == 3 && value == "'\\'") {
            is_valid = false;
            return;
        }
        number = value[1];
	}
};

struct NIZ_ZNAKOVA: terminal {
	std::shared_ptr<ppjc_type> type;
	bool is_valid;
	std::vector<char> parsed;
	void check() {
        for (size_t i = 1; i < value.size() - 1; ++i) {
            if (value[i] == '"') {
                is_valid = false;
                return;
            }
            if (value[i] == '\\') {
                if (i + 1 == value.size() - 1) {
                    is_valid = false;
                    return;
                }
                switch (value[i + 1]) {
                case 't':
                    parsed.push_back('\t');
                    break;
                case 'n':
                    parsed.push_back('\n');
                    break;
                case '0':
                    parsed.push_back('\0');
                    break;
                case '\\':
                    parsed.push_back('\\');
                    break;
                case '\'':
                    parsed.push_back('\'');
                    break;
                case '"':
                    parsed.push_back('"');
                    break;
                default:
                    is_valid = false;
                    return;
                }
                ++i;
            } else {
                parsed.push_back(value[i]);
            }
        }
        parsed.push_back('\0');

        type = std::make_shared<array_type>(cchar_t);
        is_valid = true;
	}
};

struct KR_BREAK: terminal {
	void check() {

	}
};

struct KR_CHAR: terminal {
    std::shared_ptr<primitive_type> type;

	void check() {
        type = std::make_shared<primitive_type>(char_t);
	}
};

struct KR_CONST: terminal {
	void check() {

	}
};

struct KR_CONTINUE: terminal {
	void check() {

	}
};

struct KR_ELSE: terminal {
	void check() {

	}
};

struct KR_FOR: terminal {
	void check() {

	}
};

struct KR_IF: terminal {
	void check() {

	}
};

struct KR_INT: terminal {
    std::shared_ptr<primitive_type> type;

	void check() {
        type = std::make_shared<primitive_type>(int_t);
	}
};

struct KR_RETURN: terminal {
	void check() {

	}
};

struct KR_VOID: terminal {
    std::shared_ptr<primitive_type> type;

	void check() {
        type = std::make_shared<primitive_type>(void_t);
	}
};

struct KR_WHILE: terminal {
	void check() {

	}
};

struct PLUS: terminal {
	void check() {

	}
};

struct OP_INC: terminal {
	void check() {

	}
};

struct MINUS: terminal {
	void check() {

	}
};

struct OP_DEC: terminal {
	void check() {

	}
};

struct OP_PUTA: terminal {
	void check() {

	}
};

struct OP_DIJELI: terminal {
	void check() {

	}
};

struct OP_MOD: terminal {
	void check() {

	}
};

struct OP_PRIDRUZI: terminal {
	void check() {

	}
};

struct OP_LT: terminal {
	void check() {

	}
};

struct OP_LTE: terminal {
	void check() {

	}
};

struct OP_GT: terminal {
	void check() {

	}
};

struct OP_GTE: terminal {
	void check() {

	}
};

struct OP_EQ: terminal {
	void check() {

	}
};

struct OP_NEQ: terminal {
	void check() {

	}
};

struct OP_NEG: terminal {
	void check() {

	}
};

struct OP_TILDA: terminal {
	void check() {

	}
};

struct OP_I: terminal {
	void check() {

	}
};

struct OP_ILI: terminal {
	void check() {

	}
};

struct OP_BIN_I: terminal {
	void check() {

	}
};

struct OP_BIN_ILI: terminal {
	void check() {

	}
};

struct OP_BIN_XILI: terminal {
	void check() {

	}
};

struct ZAREZ: terminal {
	void check() {

	}
};

struct TOCKAZAREZ: terminal {
	void check() {

	}
};

struct L_ZAGRADA: terminal {
	void check() {

	}
};

struct D_ZAGRADA: terminal {
	void check() {

	}
};

struct L_UGL_ZAGRADA: terminal {
	void check() {

	}
};

struct D_UGL_ZAGRADA: terminal {
	void check() {

	}
};

struct L_VIT_ZAGRADA: terminal {
	void check() {

	}
};

struct D_VIT_ZAGRADA: terminal {
	void check() {

	}
};
