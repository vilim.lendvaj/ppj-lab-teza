#!/bin/bash

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

bold=$(tput bold)
normal=$(tput sgr0)

for d in tests/*/; do
    echo "Testing $d"
    
	python process-linux.py "$d""test.c"

    res=`diff "$d""test.out" "$d""test.myout"`
    if [ "$res" != "" ]
    then
	echo "${bold}${red}FAIL${reset}${normal}"
	#echo $res
    else
        echo "${bold}${green}OK :)${reset}${normal}"
    fi
done
