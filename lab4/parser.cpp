#include <exception>
#include <iostream>
#include "terminals.h"
#include "expressions.h"
#include "statements.h"
#include "decl_def.h"
#include "parser.h"

using namespace std;

shared_ptr<tree_node> _make_node(string name) {
	if (name == "IDN") {
		return make_shared<IDN>();
	} else if (name == "BROJ") {
		return make_shared<BROJ>();
	} else if (name == "ZNAK") {
		return make_shared<ZNAK>();
	} else if (name == "NIZ_ZNAKOVA") {
		return make_shared<NIZ_ZNAKOVA>();
	} else if (name == "KR_BREAK") {
		return make_shared<KR_BREAK>();
	} else if (name == "KR_CHAR") {
		return make_shared<KR_CHAR>();
	} else if (name == "KR_CONST") {
		return make_shared<KR_CONST>();
	} else if (name == "KR_CONTINUE") {
		return make_shared<KR_CONTINUE>();
	} else if (name == "KR_ELSE") {
		return make_shared<KR_ELSE>();
	} else if (name == "KR_FOR") {
		return make_shared<KR_FOR>();
	} else if (name == "KR_IF") {
		return make_shared<KR_IF>();
	} else if (name == "KR_INT") {
		return make_shared<KR_INT>();
	} else if (name == "KR_RETURN") {
		return make_shared<KR_RETURN>();
	} else if (name == "KR_VOID") {
		return make_shared<KR_VOID>();
	} else if (name == "KR_WHILE") {
		return make_shared<KR_WHILE>();
	} else if (name == "PLUS") {
		return make_shared<PLUS>();
	} else if (name == "OP_INC") {
		return make_shared<OP_INC>();
	} else if (name == "MINUS") {
		return make_shared<MINUS>();
	} else if (name == "OP_DEC") {
		return make_shared<OP_DEC>();
	} else if (name == "OP_PUTA") {
		return make_shared<OP_PUTA>();
	} else if (name == "OP_DIJELI") {
		return make_shared<OP_DIJELI>();
	} else if (name == "OP_MOD") {
		return make_shared<OP_MOD>();
	} else if (name == "OP_PRIDRUZI") {
		return make_shared<OP_PRIDRUZI>();
	} else if (name == "OP_LT") {
		return make_shared<OP_LT>();
	} else if (name == "OP_LTE") {
		return make_shared<OP_LTE>();
	} else if (name == "OP_GT") {
		return make_shared<OP_GT>();
	} else if (name == "OP_GTE") {
		return make_shared<OP_GTE>();
	} else if (name == "OP_EQ") {
		return make_shared<OP_EQ>();
	} else if (name == "OP_NEQ") {
		return make_shared<OP_NEQ>();
	} else if (name == "OP_NEG") {
		return make_shared<OP_NEG>();
	} else if (name == "OP_TILDA") {
		return make_shared<OP_TILDA>();
	} else if (name == "OP_I") {
		return make_shared<OP_I>();
	} else if (name == "OP_ILI") {
		return make_shared<OP_ILI>();
	} else if (name == "OP_BIN_I") {
		return make_shared<OP_BIN_I>();
	} else if (name == "OP_BIN_ILI") {
		return make_shared<OP_BIN_ILI>();
	} else if (name == "OP_BIN_XILI") {
		return make_shared<OP_BIN_XILI>();
	} else if (name == "ZAREZ") {
		return make_shared<ZAREZ>();
	} else if (name == "TOCKAZAREZ") {
		return make_shared<TOCKAZAREZ>();
	} else if (name == "L_ZAGRADA") {
		return make_shared<L_ZAGRADA>();
	} else if (name == "D_ZAGRADA") {
		return make_shared<D_ZAGRADA>();
	} else if (name == "L_UGL_ZAGRADA") {
		return make_shared<L_UGL_ZAGRADA>();
	} else if (name == "D_UGL_ZAGRADA") {
		return make_shared<D_UGL_ZAGRADA>();
	} else if (name == "L_VIT_ZAGRADA") {
		return make_shared<L_VIT_ZAGRADA>();
	} else if (name == "D_VIT_ZAGRADA") {
		return make_shared<D_VIT_ZAGRADA>();
	} else if (name == "<primarni_izraz>") {
		return make_shared<primarni_izraz>();
	} else if (name == "<postfiks_izraz>") {
		return make_shared<postfiks_izraz>();
	} else if (name == "<lista_argumenata>") {
		return make_shared<lista_argumenata>();
	} else if (name == "<unarni_izraz>") {
		return make_shared<unarni_izraz>();
	} else if (name == "<unarni_operator>") {
		return make_shared<unarni_operator>();
	} else if (name == "<cast_izraz>") {
		return make_shared<cast_izraz>();
	} else if (name == "<ime_tipa>") {
		return make_shared<ime_tipa>();
	} else if (name == "<specifikator_tipa>") {
		return make_shared<specifikator_tipa>();
	} else if (name == "<multiplikativni_izraz>") {
		return make_shared<multiplikativni_izraz>();
	} else if (name == "<aditivni_izraz>") {
		return make_shared<aditivni_izraz>();
	} else if (name == "<odnosni_izraz>") {
		return make_shared<odnosni_izraz>();
	} else if (name == "<jednakosni_izraz>") {
		return make_shared<jednakosni_izraz>();
	} else if (name == "<bin_i_izraz>") {
		return make_shared<bin_i_izraz>();
	} else if (name == "<bin_xili_izraz>") {
		return make_shared<bin_xili_izraz>();
	} else if (name == "<bin_ili_izraz>") {
		return make_shared<bin_ili_izraz>();
	} else if (name == "<log_i_izraz>") {
		return make_shared<log_i_izraz>();
	} else if (name == "<log_ili_izraz>") {
		return make_shared<log_ili_izraz>();
	} else if (name == "<izraz_pridruzivanja>") {
		return make_shared<izraz_pridruzivanja>();
	} else if (name == "<izraz>") {
		return make_shared<izraz>();
	} else if (name == "<slozena_naredba>") {
		return make_shared<slozena_naredba>();
	} else if (name == "<lista_naredbi>") {
		return make_shared<lista_naredbi>();
	} else if (name == "<naredba>") {
		return make_shared<naredba>();
	} else if (name == "<izraz_naredba>") {
		return make_shared<izraz_naredba>();
	} else if (name == "<naredba_grananja>") {
		return make_shared<naredba_grananja>();
	} else if (name == "<naredba_petlje>") {
		return make_shared<naredba_petlje>();
	} else if (name == "<naredba_skoka>") {
		return make_shared<naredba_skoka>();
	} else if (name == "<prijevodna_jedinica>") {
		return make_shared<prijevodna_jedinica>();
	} else if (name == "<vanjska_deklaracija>") {
		return make_shared<vanjska_deklaracija>();
	} else if (name == "<definicija_funkcije>") {
		return make_shared<definicija_funkcije>();
	} else if (name == "<lista_parametara>") {
		return make_shared<lista_parametara>();
	} else if (name == "<deklaracija_parametra>") {
		return make_shared<deklaracija_parametra>();
	} else if (name == "<lista_deklaracija>") {
		return make_shared<lista_deklaracija>();
	} else if (name == "<deklaracija>") {
		return make_shared<deklaracija>();
	} else if (name == "<lista_init_deklaratora>") {
		return make_shared<lista_init_deklaratora>();
	} else if (name == "<init_deklarator>") {
		return make_shared<init_deklarator>();
	} else if (name == "<izravni_deklarator>") {
		return make_shared<izravni_deklarator>();
	} else if (name == "<inicijalizator>") {
		return make_shared<inicijalizator>();
	} else if (name == "<lista_izraza_pridruzivanja>") {
		return make_shared<lista_izraza_pridruzivanja>();
	} else {
		throw invalid_argument("nepostojeci znak");
	}
}

shared_ptr<tree_node> make_node(shared_ptr<prijevodna_jedinica> root, shared_ptr<tree_node> parent, string str, int id) {
	string name = str.substr(0, str.find(' '));
	shared_ptr<tree_node> node = _make_node(name);
	node->root = root;
	node->parent = parent;
	node->str = str;
	node->id = id;
	node->init();
	return node;
}
