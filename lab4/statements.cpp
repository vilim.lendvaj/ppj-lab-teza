#include <cassert>

#include "terminals.h"
#include "expressions.h"
#include "statements.h"
#include "decl_def.h"

//using namespace std;

void slozena_naredba::enclose_child(std::shared_ptr<tree_node> child) {
    if (parent == enclosing_function) {
        child->enclosing_scope = enclosing_function;
    } else {
        child->enclosing_scope = shared_from_this();
    }
    child->enclosing_function = enclosing_function;
    child->enclosing_block = shared_from_this();
    child->enclosing_loop = enclosing_loop;
}

void slozena_naredba::check() {
    if (children.size() == 3) {
        //<slozena_naredba> ::= L_VIT_ZAGRADA <lista_naredbi> D_VIT_ZAGRADA
        children[1]->check();
        assert(locals_cnt == 0);
    } else if (children.size() == 4) {
        //<slozena_naredba> ::= L_VIT_ZAGRADA <lista_deklaracija> <lista_naredbi> D_VIT_ZAGRADA
        children[1]->check();
        children[2]->check();

        root->fout<<"\t\tADD R7, "<<locals_cnt * 4<<", R7\n";
    }
}

void lista_naredbi::check() {
    if (children.size() == 1) {
        //<lista_naredbi> ::= <naredba>
        children[0]->check();
    } else if (children.size() == 2){
        //<lista_naredbi> ::= <lista_naredbi> <naredba>
        children[0]->check();
        children[1]->check();
    }
}

void naredba::check() {
    //<naredba> ::= (<slozena_naredba> | <izraz_naredba> | <naredba_grananja> | <naredba_petlje> | <naredba_skoka>)
    children[0]->check();
}

void izraz_naredba::check() {
    if (children.size() == 1) {
        //<izraz_naredba> ::= TOCKAZAREZ
        type = std::make_shared<primitive_type>(int_t);
    }
    else if (children.size() == 2) {
        //<izraz_naredba> ::= <izraz> TOCKAZAREZ
        auto izraz_naredba_child = std::dynamic_pointer_cast<izraz>(children[0]);
        izraz_naredba_child->check();

        type = izraz_naredba_child->type;

        if (*type != void_t) {
            root->fout<<"\t\tPOP R0\n";
        }
    }
}

void naredba_grananja::enclose_child(std::shared_ptr<tree_node> child) {
    child->enclosing_scope = enclosing_scope;
    child->enclosing_function = enclosing_function;
    child->enclosing_block = enclosing_block;
    child->enclosing_loop = enclosing_loop;
}

void naredba_grananja::check() {
    if (children.size() == 5) {
        //<naredba_grananja> ::= KR_IF L_ZAGRADA <izraz> D_ZAGRADA <naredba>
        auto izraz_child = std::dynamic_pointer_cast<izraz>(children[2]);
        izraz_child->check();

        if(!izraz_child->type->is_convertible(int_t))
            error();

        root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tCMP R0, 0\n";
        root->fout<<"\t\tJR_EQ ENDIF_"<<id<<"\n";

        auto naredba_child = std::dynamic_pointer_cast<naredba>(children[4]);
        naredba_child->check();

        root->fout<<"ENDIF_"<<id<<"\tMOVE R0, R0\n";

    }
    else if (children.size() == 7) {
        //<naredba_grananja> ::= KR_IF L_ZAGRADA <izraz> D_ZAGRADA <naredba> KR_ELSE <naredba>
        auto izraz_child = std::dynamic_pointer_cast<izraz>(children[2]);
        izraz_child->check();


        if (!izraz_child->type->is_convertible(int_t))
            error();

        root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tCMP R0, 0\n";
        root->fout<<"\t\tJR_EQ ELSEIF_"<<id<<"\n";

        auto naredba_if_child = std::dynamic_pointer_cast<naredba>(children[4]);
        naredba_if_child->check();
        root->fout<<"\t\tJP ENDIF_"<<id<<"\n";
        root->fout<<"ELSEIF_"<<id<<"\tMOVE R0, R0\n";
        auto naredba_else_child = std::dynamic_pointer_cast<naredba>(children[6]);
        naredba_else_child->check();
        root->fout<<"ENDIF_"<<id<<"\tMOVE R0, R0\n";
    }
}

void naredba_petlje::enclose_child(std::shared_ptr<tree_node> child) {
    child->enclosing_scope = enclosing_scope;
    child->enclosing_function = enclosing_function;
    child->enclosing_block = enclosing_block;
    child->enclosing_loop = shared_from_this();
}

void naredba_petlje::check() {
    if (children.size() == 5) {
        //<naredba_petlje> ::= KR_WHILE L_ZAGRADA <izraz> D_ZAGRADA <naredba>
        auto izraz_child = std::dynamic_pointer_cast<izraz>(children[2]);

        root->fout<<"LOOP_"<<id<<"\tMOVE R0, R0\n";
        izraz_child->check();

        if (!izraz_child->type->is_convertible(int_t))
            error();

        root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tCMP R0, 0\n";
        root->fout<<"\t\tJR_EQ ENDLOOP_"<<id<<"\n";

        auto naredba_child = std::dynamic_pointer_cast<naredba>(children[4]);
        naredba_child->check();

        root->fout<<"\t\tJR LOOP_"<<id<<"\n";
        root->fout<<"ENDLOOP_"<<id<<"\tMOVE R0, R0\n";
    }
    else if (children.size() == 6) {
        //<naredba_petlje> ::= KR_FOR L_ZAGRADA <izraz_naredba> <izraz_naredba> D_ZAGRADA <naredba>
        auto izraz_naredba_1_child = std::dynamic_pointer_cast<izraz_naredba>(children[2]);
        izraz_naredba_1_child->check();

        root->fout<<"LOOP_"<<id<<"\tMOVE R0, R0\n";

        auto izraz_naredba_2_child = std::dynamic_pointer_cast<izraz_naredba>(children[3]);
        izraz_naredba_2_child->check();

        if(!izraz_naredba_2_child->type->is_convertible(int_t))
            error();

        //root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tCMP R0, 0\n";
        root->fout<<"\t\tJR_EQ ENDLOOP_"<<id<<"\n";

        auto naredba_child = std::dynamic_pointer_cast<naredba>(children[5]);
        naredba_child->check();

        root->fout<<"\t\tJR LOOP_"<<id<<"\n";
        root->fout<<"ENDLOOP_"<<id<<"\tMOVE R0, R0\n";
    }
    else if (children.size() == 7) {
        //<naredba_petlje> ::= KR_FOR L_ZAGRADA <izraz_naredba> <izraz_naredba> <izraz> D_ZAGRADA <naredba>
        auto izraz_naredba_1_child = std::dynamic_pointer_cast<izraz_naredba>(children[2]);
        izraz_naredba_1_child->check();

        root->fout<<"LOOP_"<<id<<"\tMOVE R0, R0\n";

        auto izraz_naredba_2_child = std::dynamic_pointer_cast<izraz_naredba>(children[3]);
        izraz_naredba_2_child->check();

        if(!izraz_naredba_2_child->type->is_convertible(int_t))
            error();

        //root->fout<<"\t\tPOP R0\n";
        root->fout<<"\t\tCMP R0, 0\n";
        root->fout<<"\t\tJR_EQ ENDLOOP_"<<id<<"\n";

        auto naredba_child = std::dynamic_pointer_cast<naredba>(children[6]);
        naredba_child->check();

        auto izraz_child = std::dynamic_pointer_cast<izraz>(children[4]);
        izraz_child->check();

        root->fout<<"\t\tJR LOOP_"<<id<<"\n";
        root->fout<<"ENDLOOP_"<<id<<"\tMOVE R0, R0\n";
    }
}

void naredba_skoka::check() {
    if (children.size() == 3) {
        //<naredba_skoka> ::= KR_RETURN <izraz> TOCKAZAREZ
        auto child = std::dynamic_pointer_cast<izraz>(children[1]);
        child->check();

        if (!enclosing_function || !child->type->is_convertible(enclosing_function->type->return_type))
            error();

        root->fout<<"\t\tPOP R5\n";
        root->fout<<"\t\tJR R_"<<enclosing_function->id<<"\n";
    }
    else if (children.size() == 2 && typeid(*children[0]) == typeid(KR_RETURN)){
        //<naredba_skoka> ::= KR_RETURN TOCKAZAREZ
        if (!enclosing_function || enclosing_function->type->return_type != void_t)
            error();

        root->fout<<"\t\tJR R_"<<enclosing_function->id<<"\n";
    }
    else if (children.size() == 2 && (typeid(*children[0]) == typeid(KR_CONTINUE) || typeid(*children[0]) == typeid(KR_BREAK))){
        //<naredba_skoka> ::= (KR_CONTINUE | KR_BREAK) TOCKAZAREZ
        if (!enclosing_loop)
            error();

        int locals_cnt_sum = 0;
        std::shared_ptr<tree_node> tmp_node = parent;
        while (tmp_node != enclosing_loop) {
            if(typeid(*tmp_node) == typeid(slozena_naredba)){
                auto current_node = std::dynamic_pointer_cast<slozena_naredba>(tmp_node);
                locals_cnt_sum += current_node->locals_cnt;
            }
            tmp_node = tmp_node->parent;
        }

        root->fout<<"\t\tADD R7, "<<locals_cnt_sum * 4<<", R7\n";

        if(typeid(*children[0]) == typeid(KR_CONTINUE)){
            root->fout<<"\t\tJR LOOP_"<<enclosing_loop->id<<"\n";
        }
        else if(typeid(*children[0]) == typeid(KR_BREAK)){
            root->fout<<"\t\tJR ENDLOOP_"<<enclosing_loop->id<<"\n";
        }
    }
}

void prijevodna_jedinica::enclose_child(std::shared_ptr<tree_node> child) {
    if (root) {
        child->enclosing_scope = root;
    } else {
        child->enclosing_scope = shared_from_this();
    }
    child->enclosing_function = nullptr;
    child->enclosing_block = nullptr;
    child->enclosing_loop = nullptr;
}

void prijevodna_jedinica::check() {
    //<prijevodna_jedinica> ::= <vanjska_deklaracija>
    if(children.size() == 1){
        children[0]->check();
    }
    //<prijevodna_jedinica> ::= <prijevodna_jedinica> <vanjska_deklaracija>
    else if(children.size() == 2) {
        children[0]->check();
        children[1]->check();
    }
}

void vanjska_deklaracija::check() {
    //<vanjska_deklaracija> ::= (<definicija_funkcije> | <deklaracija>)
    if(children.size() == 1) {
        children[0]->check();
    }
}

