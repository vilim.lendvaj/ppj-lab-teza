#pragma once


static bool starts_with(const std::string str, const std::string prefix)
{
    return ((prefix.size() <= str.size()) && std::equal(prefix.begin(), prefix.end(), str.begin()));
}

static std::string last_word(const std::string str){
    return str.substr(str.find_last_of(' ', str.size())+1);
}
