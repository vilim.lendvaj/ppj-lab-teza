#pragma once
#include "tree.h"

std::shared_ptr<tree_node> make_node(std::shared_ptr<prijevodna_jedinica> root, std::shared_ptr<tree_node> parent, std::string str, int id);
