#include <iostream>
#include <cassert>

#include "parser.h"
#include "statements.h"

//using namespace std;

void tree_node::cleanup() {
    root->fout.close();
}

int main()
{
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(nullptr);

	std::string str;
	getline(std::cin, str);
	assert(str == "<prijevodna_jedinica>");
	std::shared_ptr<prijevodna_jedinica> root = std::dynamic_pointer_cast<prijevodna_jedinica>(make_node(nullptr, nullptr, str, 1));
	root->declared_functions.emplace_back("main", function_type(int_t, std::vector<std::shared_ptr<ppjc_type>>{}));
	std::shared_ptr<tree_node> node = root;
	int level = 1, id = 2;

	while (getline(std::cin, str) && !str.empty()) {
		int new_level = str.find_first_not_of(" ");
		while (new_level < level) {
			//cout<<node->str<<": "<<node->children.size()<<'\n';
			node = node->parent;
			--level;
		}
		node->children.push_back(make_node(root, node, str.substr(level), id));
		node = node->children.back();
		++level;
		++id;
	}

	root->fout.open("a.frisc");
	root->fout<<"\t\tMOVE 40000, R7\n";
	root->fout<<"\t\tCALL INIT\n";
	root->fout<<"\t\tCALL F_main\n";
	root->fout<<"\t\tHALT\n";
	root->calculate_scopes();
	root->check();

	for (const auto& x: root->declared_functions) {
        auto *found = root->find(x.first);
        if (!found
            || typeid(*found->type) != typeid(function_type)
            || *std::dynamic_pointer_cast<function_type>(found->type) != x.second
            || !found->defined) {

            std::cout<<(x.first == "main" ? "main" : "funkcija")<<std::endl;
            exit(0);
        }
	}

	root->fout<<"INIT\t MOVE R0, R0\n";
	root->fout<<root->init.str();
	root->fout<<"\t\tRET\n";

    root->fout<<"OP_MOD          LOAD R0, (R7+8)\n"
                "                LOAD R1, (R7+4)\n"
                "                MOVE %D 0, R2\n"

                "OP_MOD_TEST1    AND R0, R0, R0\n"
                "                JR_P OP_MOD_TEST2\n"

                "OP_MOD_NEG1     XOR R0, -1, R0\n"
                "                ADD R0, 1 ,R0\n"
                "                MOVE %D 1, R2\n"

                "OP_MOD_TEST2    AND R1, R1, R1\n"
                "                JR_P OP_MOD_LOOP\n"

                "OP_MOD_NEG2     XOR R1, -1, R1\n"
                "                ADD R1, 1 ,R1\n"

                "OP_MOD_LOOP     SUB R0, R1, R0\n"
                "                JP_Z OP_MOD_NEGCHECK\n"
                "                JP_P OP_MOD_LOOP\n"
                "                ADD R0, R1, R0\n"

                "OP_MOD_NEGCHECK AND R2, R2, R2\n"
                "                JP_Z OP_MOD_FINISH\n"

                "                XOR R0, -1, R0\n"
                "                ADD R0, 1 ,R0\n"

                "OP_MOD_FINISH   MOVE R0, R6\n"
                "                RET\n";


    root->fout<<"OP_DIV          LOAD R0, (R7+8)\n"
                "                LOAD R1, (R7+4)\n"
                "                MOVE %D 0, R2\n"

                "                XOR R0, R1, R3\n"

                "OP_DIV_TEST1    AND R0, R0, R0\n"
                "                JR_P OP_DIV_TEST2\n"

                "OP_DIV_NEG1     XOR R0, -1, R0\n"
                "                ADD R0, 1 ,R0\n"

                "OP_DIV_TEST2    AND R1, R1, R1\n"
                "                JR_P OP_DIV_TESTNULL\n"

                "OP_DIV_NEG2     XOR R1, -1, R1\n"
                "                ADD R1, 1, R1\n"

                "OP_DIV_TESTNULL AND R1, R1, R1\n"
                "                JR_Z OP_DIV_FINISH\n"



                "OP_DIV_LOOP     ADD R2, 1, R2\n"
                "                SUB R0, R1, R0\n"
                "                JP_Z OP_DIV_TESTSIGN\n"
                "                JP_P OP_DIV_LOOP\n"

                "                SUB R2, 1, R2\n"

                "OP_DIV_TESTSIGN ROTL R3, 1, R3\n"
                "                JR_NC OP_DIV_FINISH\n"

                "OP_DIV_SIGN     XOR R2, -1, R2\n"
                "                ADD R2, 1, R2\n"

                "OP_DIV_FINISH   MOVE R2, R6\n"
                "                RET\n";

    root->fout<<"OP_MUL          LOAD R0, (R7+8)\n"
                "                LOAD R1, (R7+4)\n"
                "                MOVE %D 0, R2\n"

                "                XOR R0, R1, R3\n"

                "OP_MUL_TEST1    AND R0, R0, R0\n"
                "                JR_P OP_MUL_TEST2\n"

                "OP_MUL_NEG1     XOR R0, -1, R0\n"
                "                ADD R0, 1, R0\n"

                "OP_MUL_TEST2    AND R1, R1, R1\n"
                "                JR_P OP_MUL_TESTNULL\n"

                "OP_MUL_NEG2     XOR R1, -1, R1\n"
                "                ADD R1, 1, R1\n"

                "OP_MUL_TESTNULL AND R1, R1, R1\n"
                "                JP_Z OP_MUL_FINISH\n"

                "OP_MUL_LOOP     ADD R0, R2, R2\n"
                "                SUB R1, 1, R1\n"
                "                JP_NZ OP_MUL_LOOP\n"

                "OP_MUL_TESTSIGN ROTL R3, 1, R3\n"
                "                JR_NC OP_MUL_FINISH\n"

                "OP_MUL_SIGN     XOR R2, -1, R2\n"
                "                ADD R2, 1, R2\n"

                "OP_MUL_FINISH   MOVE R2, R6\n"
                "                RET\n";



    root->fout.close();
    //std::cout<<"all ok"<<std::endl;
    return 0;
}

