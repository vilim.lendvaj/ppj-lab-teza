#pragma once
#include "tree.h"

struct definicija_funkcije: scope, std::enable_shared_from_this<definicija_funkcije> {
    std::shared_ptr<function_type> type;
    int locals_cnt;

    void enclose_child(std::shared_ptr<tree_node> child) override;
	void check();
};

struct lista_parametara: tree_node {
    std::vector<std::shared_ptr<ppjc_type>> types;
    std::vector<std::string> names;
	void check();
};

struct deklaracija_parametra: tree_node {
    std::shared_ptr<ppjc_type> type;
    std::string name;
	void check();
};

struct lista_deklaracija: tree_node {
	void check();
};

struct deklaracija: tree_node {
	void check();
};

struct lista_init_deklaratora: tree_node {
    std::shared_ptr<primitive_type> ntype;
	void check();
};

struct init_deklarator: tree_node {
    std::shared_ptr<primitive_type> ntype;
	void check();
};

struct izravni_deklarator: tree_node {
    std::shared_ptr<primitive_type> ntype;
    std::shared_ptr<ppjc_type> type;
    int br_elem;
    std::string value;
    symbol_entry *entry;
    int array_offset;

	void check();
};

struct inicijalizator: tree_node {
    std::shared_ptr<ppjc_type> type;
    std::vector<std::shared_ptr<ppjc_type>> types;
    int br_elem;
	void check();
};

struct lista_izraza_pridruzivanja: tree_node {
    std::vector<::std::shared_ptr<ppjc_type>> types;
    int br_elem;
	void check();
};

