#pragma once
#include <unordered_map>
#include <iostream>
#include <memory>

#include "types.h"

struct tree_node;

struct symbol_entry {
	std::shared_ptr<ppjc_type> type;
	bool defined;
	bool global;
	int offset;
};

struct scope;
struct prijevodna_jedinica;
struct definicija_funkcije;
struct slozena_naredba;
struct naredba_petlje;

struct tree_node {
	std::string str;
    std::shared_ptr<tree_node> parent;
	std::shared_ptr<prijevodna_jedinica> root;
	std::shared_ptr<scope> enclosing_scope;
	std::shared_ptr<definicija_funkcije> enclosing_function;
	std::shared_ptr<slozena_naredba> enclosing_block;
	std::shared_ptr<naredba_petlje> enclosing_loop;
	std::vector<std::shared_ptr<tree_node>> children;
	int id;

	virtual void init() {}

	virtual void check() = 0;

	virtual std::string to_string() {
	    return str;
	}

	virtual void enclose_child(std::shared_ptr<tree_node> child) {
	    child->enclosing_scope = enclosing_scope;
	    child->enclosing_function = enclosing_function;
	    child->enclosing_block = enclosing_block;
	    child->enclosing_loop = enclosing_loop;
	}

	void calculate_scopes() {
        for (const auto& child: children) {
            enclose_child(child);
            child->calculate_scopes();
        }
	}

	void error() {
        std::cout<<str<<" ::=";
        for (const auto& child: children) {
            std::cout<<' '<<child->to_string();
        }
        std::cout<<std::endl;
        cleanup();
        exit(0);
	}

	void cleanup();
};

struct scope: tree_node {
	std::unordered_map<std::string, symbol_entry> symbol_table;

	symbol_entry* find(const std::string& name) {
		scope* node = this;
		int indent = 0;
		while (node) {
            //std::cout<<std::string(indent, ' ')<<node->str<<std::endl;
            ++indent;
			auto it = node->symbol_table.find(name);
			if (it != node->symbol_table.end())
				return &it->second;
			node = node->enclosing_scope.get();
		}
		return nullptr;
	}

	bool is_local(const std::string& name) {
        return symbol_table.count(name);
	}

	symbol_entry* declare(const std::string& name, symbol_entry entry) {
	    //std::cout<<"declared "<<name<<std::endl;
        return &symbol_table.emplace(name, entry).first->second;
	}
};
