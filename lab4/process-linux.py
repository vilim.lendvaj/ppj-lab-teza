from sys import argv, stderr
from pathlib import Path
from subprocess import run, PIPE
import os

src = Path(argv[1])
root = Path('..')

if len(argv) > 2:
    with open('ppjC.lan') as f:
        run(['python', 'GLA.py'], cwd = root / 'lab1', stdin = f)

with src.open() as fin, src.with_suffix('.la').open('w') as fout:
    run(['python','LA.py'], cwd = root / 'lab1' / 'analizator', stdin = fin, stdout = fout)
    

if len(argv) > 2:
    with open('ppjC.san') as f:
        run(['python','GSA.py'], cwd = root / 'lab2', stdin = f)

with src.with_suffix('.la').open() as fin, src.with_suffix('.sa').open('w') as fout:
    run(['python','SA.py'], cwd = root / 'lab2' / 'analizator', stdin = fin, stdout = fout)

if len(argv) > 2:
    run(['rm','-r','build'], cwd = root / 'lab3')
    run(['mkdir','build'], cwd = root / 'lab3')
    run(['cmake','..'], cwd = root / 'lab3' / 'build')
    run(['make','-j4'], cwd = root / 'lab3' / 'build')

with src.with_suffix('.sa').open() as f:
    proc = run(['./lab3'], cwd = root / 'lab3' / 'build', stdin = f)
    proc.check_returncode()

if len(argv) > 2:
    run(['rm','-r','build'], cwd = root / 'lab4')
    run(['mkdir','build'], cwd = root / 'lab4')
    run(['cmake','..'], cwd = root / 'lab4' / 'build')
    run(['make','-j4'], cwd = root / 'lab4' / 'build')

with src.with_suffix('.sa').open() as fin:
    run(['./lab4'], cwd = root / 'lab4' / 'build', stdin = fin)

with src.with_suffix('.myout').open('w') as fout, open(os.devnull, 'w') as devnull:
    run(['node','frisc-console.js', '../build/a.frisc'],  cwd = root / 'lab4' / 'frisc', stdout = fout, stderr=devnull)