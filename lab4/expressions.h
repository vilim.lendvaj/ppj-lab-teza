#pragma once
#include "tree.h"

struct primarni_izraz: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;

	void check();
};

struct postfiks_izraz: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;
    bool is_array_indexing;

	void check();
};

struct lista_argumenata: tree_node {
    std::vector<std::shared_ptr<ppjc_type>> types;
	void check();
};

struct unarni_izraz: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;

	void check();
};

struct unarni_operator: tree_node {
    std::string name;

	void check();
};

struct cast_izraz: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;

	void check();
};

struct ime_tipa: tree_node {
    std::shared_ptr<primitive_type> type;
	void check();
};

struct specifikator_tipa: tree_node {
    std::shared_ptr<primitive_type> type;
	void check();
};

struct multiplikativni_izraz: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;

	void check();
};

struct aditivni_izraz: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;

	void check();
};

struct odnosni_izraz: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;

	void check();
};

struct jednakosni_izraz: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;

	void check();
};

struct bin_i_izraz: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;

	void check();
};

struct bin_xili_izraz: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;

	void check();
};

struct bin_ili_izraz: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;

	void check();
};

struct log_i_izraz: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;

	void check();
};

struct log_ili_izraz: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;

	void check();
};

struct izraz_pridruzivanja: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;

	void check();
};

struct izraz: tree_node {
    std::shared_ptr<ppjc_type> type;
    bool lvalue;
    std::string value;
    symbol_entry *entry;

	void check();
};

